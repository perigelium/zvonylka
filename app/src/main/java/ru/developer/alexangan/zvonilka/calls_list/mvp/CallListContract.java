package ru.developer.alexangan.zvonilka.calls_list.mvp;

import ru.developer.alexangan.zvonilka.base.mvp.MvpPresenter;
import ru.developer.alexangan.zvonilka.base.mvp.MvpView;

public interface CallListContract
{
    interface View extends MvpView
    {
        void showMessage(int messageResId);
    }

    interface Presenter extends MvpPresenter<View>
    {

    }
}
