package ru.developer.alexangan.zvonilka.login.mvp;

import ru.developer.alexangan.zvonilka.base.mvp.MvpPresenter;
import ru.developer.alexangan.zvonilka.base.mvp.MvpView;

public interface LoginContract
{
    interface View extends MvpView
    {
        String getLoginText();

        String getPasswordText();

        void focusSubmitLoginPassword();

        void focusLogin();

        void focusPassword();

        void clearLoginField();

        void clearPasswordField();

        void showMessage(int messageResId);

        // go to next screen
        void next();

        // close screen
        void close();

        void showTextView(int resId, int stringId, boolean alert);

        void showView(int resId, int drawableId);

        void hideView(int resId);

        void focusRootLayout();

        void setPasswordText(String password);

        void setLoginText(String login);
    }

    interface Presenter extends MvpPresenter<View>
    {
        void onSubmitLoginPasswordClicked();

        void OnTVPasswordReminderClicked();

        void OnPasswordKeycodeEnter();

        void onLoginFieldFocused();

        void onPasswordFieldFocused();

        void onLoginFieldLooseFocus();

        void onPasswordFieldLooseFocus();
    }
}
