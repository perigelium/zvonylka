package ru.developer.alexangan.zvonilka.model;

import android.net.Uri;

import java.io.Serializable;
import java.util.ArrayList;

public class ContactsBookItem implements Serializable
{
    public String nameFirstLetter;
    public String displayName;
    public ArrayList<Phone> phones;
    public ArrayList<CallEvent> calls;
    public Uri thumbPhotoUri;
    public Uri displayPhotoUri;

    public ContactsBookItem(String displayName, ArrayList<Phone> phones, Uri thumbPhotoUri, Uri displayPhotoUri)
    {
        this.displayName = displayName;
        this.phones = phones;
        this.thumbPhotoUri = thumbPhotoUri;
        this.displayPhotoUri = displayPhotoUri;
    }
}
