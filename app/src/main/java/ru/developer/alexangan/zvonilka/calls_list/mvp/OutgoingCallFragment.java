package ru.developer.alexangan.zvonilka.calls_list.mvp;


import android.os.Bundle;
import android.app.Fragment;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.utils.ViewUtils;

public class OutgoingCallFragment extends Fragment implements View.OnClickListener
{
    @BindView(R.id.tvCallerFullName)
    TextView tvCallerFullName;

    @BindView(R.id.tvCallStatus)
    TextView tvCallStatus;

    @BindView(R.id.ivCallerAvatar)
    ImageView ivCallerAvatar;

    @BindView(R.id.ibFinishCall)
    ImageButton ibFinishCall;

    @BindView(R.id.flHold)
    FrameLayout flHold;

    @BindView(R.id.ibHold)
    ImageView ibHold;

    @BindView(R.id.flMuteMic)
    FrameLayout flMuteMic;

    @BindView(R.id.ibMuteMic)
    ImageView ibMuteMic;

    @BindView(R.id.flDialpad)
    FrameLayout flDialpad;

    @BindView(R.id.llCallControls)
    LinearLayout llCallControls;

    @BindView(R.id.chrono)
    Chronometer chrono;

    @BindView(R.id.etNumber)
    EditText etTypedDigit;

    @BindView(R.id.btn0)
    FrameLayout btn0;

    @BindView(R.id.btn1)
    FrameLayout btn1;

    @BindView(R.id.btn2)
    FrameLayout btn2;

    @BindView(R.id.btn3)
    FrameLayout btn3;

    @BindView(R.id.btn4)
    FrameLayout btn4;

    @BindView(R.id.btn5)
    FrameLayout btn5;

    @BindView(R.id.btn6)
    FrameLayout btn6;

    @BindView(R.id.btn7)
    FrameLayout btn7;

    @BindView(R.id.btn8)
    FrameLayout btn8;

    @BindView(R.id.btn9)
    FrameLayout btn9;

    @BindView(R.id.btnAsterisk)
    FrameLayout btnAsterisk;

    @BindView(R.id.btnHash)
    FrameLayout btnHash;

    @BindView(R.id.flDialer)
    FrameLayout flDialer;

    @BindView(R.id.ibPhoneTubeWhiteInGreen)
    ImageButton ibPhoneTubeWhiteInGreen;

    private OnFragmentInteractionListener mListener;
    private ContactsBookItem contactsBookItem;
    private boolean muteMic;
    private boolean isHold;

    public OutgoingCallFragment()
    {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.outgoing_call_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);
        muteMic = false;
        isHold = false;

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnAsterisk.setOnClickListener(this);
        btnHash.setOnClickListener(this);
        flDialpad.setOnClickListener(this);
        ibFinishCall.setOnClickListener(this);
        flHold.setOnClickListener(this);
        flMuteMic.setOnClickListener(this);

        ibPhoneTubeWhiteInGreen.setOnClickListener(this);

        etTypedDigit.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count)
            {
                String strText = charSequence.toString();

                if (strText.length() != 0)
                {
                    String lastChar = strText.substring(strText.length() - 1);

                    char[] szRes = lastChar.toCharArray(); // Convert String to Char array

                    KeyCharacterMap CharMap = KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD);

                    KeyEvent[] events = CharMap.getEvents(szRes);

                    int keyCode = events[0].getKeyCode();

                    if (lastChar.equals("*"))
                    {
                        keyCode = KeyEvent.KEYCODE_STAR;
                    }

                    if (lastChar.equals("#"))
                    {
                        keyCode = KeyEvent.KEYCODE_POUND;
                    }

                    mListener.onSendDTFMcode(keyCode);
                }
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

            }
        });

        if (getArguments() != null)
        {
            contactsBookItem = (ContactsBookItem) getArguments().getSerializable("ContactsBookItem");

            if (contactsBookItem != null)
            {
                if (contactsBookItem.displayName != null)
                {
                    tvCallerFullName.setText(contactsBookItem.displayName);
                } else
                {
                    if (contactsBookItem.phones != null && contactsBookItem.phones.get(0) != null)
                    {
                        tvCallerFullName.setText(contactsBookItem.phones.get(0).number);
                    }
                }

                //Picasso.with(getActivity()).load(contactsBookItem.photoUri).fit().into(ivCallerAvatar);

                if (contactsBookItem.displayPhotoUri != null)
                {
                    try
                    {
                        ivCallerAvatar.setImageURI(contactsBookItem.displayPhotoUri);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }

        chrono.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener()
        {
            @Override
            public void onChronometerTick(Chronometer chronometer)
            {
                long time = SystemClock.elapsedRealtime() - chronometer.getBase();
                int h = (int) (time / 3600000);
                int m = (int) (time - h * 3600000) / 60000;
                int s = (int) (time - h * 3600000 - m * 60000) / 1000;

                //String hh = h < 10 ? "0"+h: h+"";
                //if(h==0) hh = "";
                String mm = m < 10 ? "0" + m : m + "";
                String ss = s < 10 ? "0" + s : s + "";

                String t = mm + ":" + ss;
                chronometer.setText(t);
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof ru.developer.alexangan.zvonilka.calls_list.mvp.OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }

    public void setMicrofonStatus(boolean enabled)
    {
        if (!enabled)
        {
            ibMuteMic.setImageDrawable(null);
            ibMuteMic.setImageDrawable(getResources().getDrawable(R.drawable.microfon_icon_enabled));
        } else
        {
            ibMuteMic.setImageDrawable(null);
            ibMuteMic.setImageDrawable(getResources().getDrawable(R.drawable.microfon_icon_crossed));
        }
    }

    public void setStatusRinging()
    {
        tvCallStatus.setText("Ringing");
    }

    public void setStatusAnswered()
    {
        tvCallStatus.setVisibility(View.GONE);
        chrono.setVisibility(View.VISIBLE);

        chrono.setBase(SystemClock.elapsedRealtime());
        chrono.setText("00:00");
        chrono.start();
    }

    public void setStatusConnecting()
    {
        tvCallStatus.setText("Connecting");
    }

    public void setHoldStatus(boolean hold)
    {
        if (!hold)
        {
            ibMuteMic.setImageDrawable(null);
            ibHold.setImageDrawable(getResources().getDrawable(R.drawable.pause_icon_enabled));
        } else
        {
            ibMuteMic.setImageDrawable(null);
            ibHold.setImageDrawable(getResources().getDrawable(R.drawable.pause_icon_crossed));
        }
    }

    public void setStatusCallClosed()
    {
        ViewUtils.hideSoftKeyboard(getActivity());
    }

    @Override
    public void onClick(View view)
    {
        String strText;

        switch (view.getId())
        {
            case R.id.btn0:
                strText = etTypedDigit.getText() + "0";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn1:
                strText = etTypedDigit.getText() + "1";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn2:
                strText = etTypedDigit.getText() + "2";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn3:
                strText = etTypedDigit.getText() + "3";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn4:
                strText = etTypedDigit.getText() + "4";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn5:
                strText = etTypedDigit.getText() + "5";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn6:
                strText = etTypedDigit.getText() + "6";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn7:
                strText = etTypedDigit.getText() + "7";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn8:
                strText = etTypedDigit.getText() + "8";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn9:
                strText = etTypedDigit.getText() + "9";
                etTypedDigit.setText(strText);
                break;

            case R.id.btnAsterisk:
                strText = etTypedDigit.getText() + "*";
                etTypedDigit.setText(strText);
                break;

            case R.id.btnHash:
                strText = etTypedDigit.getText() + "#";
                etTypedDigit.setText(strText);
                break;

            case R.id.btnBackspace:
                strText = etTypedDigit.getText().toString();
                etTypedDigit.setText(strText.substring(0, strText.length() - 1));
                break;

            case R.id.flDialpad:
                llCallControls.setVisibility(View.INVISIBLE);
                flDialer.setVisibility(View.VISIBLE);
                break;

            case R.id.ibFinishCall:
                mListener.onUserHangUp();
                break;

            case R.id.flHold:
                isHold = !isHold;
                mListener.onUserHoldCall(isHold);
                break;

            case R.id.flMuteMic:
                muteMic = !muteMic;
                mListener.onUserMuteMicrofon(muteMic);
                break;

            case R.id.ibPhoneTubeWhiteInGreen:
                mListener.onUserHangUp();
                break;
        }
    }
}
