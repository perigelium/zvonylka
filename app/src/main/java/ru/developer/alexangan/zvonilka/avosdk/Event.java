package ru.developer.alexangan.zvonilka.avosdk;

/**
 * Created by olegrussu on 03/06/2017.
 */

public class Event {

    public static final int EVENT_HANG_UP = 0;
    public static final int EVENT_DECLINE = 1;
    public static final int EVENT_ACCEPT = 2;
    public static final int EVENT_MUTE = 3;
    public static final int EVENT_UNMUTE = 4;
    public static final int EVENT_HOLD = 5;
    public static final int EVENT_UNHOLD = 6;
    public static final int EVENT_CALL_CLOSED = 7;
    public static final int EVENT_CALL_ANSWERED = 8;

    private int mEventId;

    public Event(int eventId) {
        mEventId = eventId;
    }

    public int getEventId() {
        return mEventId;
    }
}
