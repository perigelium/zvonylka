package ru.developer.alexangan.zvonilka;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import ru.developer.alexangan.zvonilka.login.mvp.LoginActivity;
import ru.developer.alexangan.zvonilka.storage.Preferences;

import static ru.developer.alexangan.zvonilka.common.Constants.PERMISSION_CONSTANTS;

public class StartActivity extends Activity
{
    private int PERMISSION_REQUEST_CODE = 11;
    private static boolean RECREATE_ACTIVITY = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        if (!RECREATE_ACTIVITY && savedInstanceState != null)
        {
            this.finish();
        }

        if (RECREATE_ACTIVITY)
        {
            RECREATE_ACTIVITY = false;
        }

        boolean allPermissionsGained = isAllRequestedPermissionsGranted(PERMISSION_CONSTANTS);

        if (!allPermissionsGained)
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            {
                requestMultiplePermissions(PERMISSION_CONSTANTS);
            }
        }

        if (allPermissionsGained)
        {
            Preferences preferences = Preferences.getInstance();
            preferences.init(this);

            LoginActivity.checkLogin(this);
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);

        RECREATE_ACTIVITY = true;
        this.recreate();
    }

    @TargetApi(Build.VERSION_CODES.M)
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void requestMultiplePermissions(String[] permissions)
    {
        requestPermissions(permissions, PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (requestCode == PERMISSION_REQUEST_CODE)
        {
            boolean onePermissionNotGranted = false;

            for (int i = 0; i < grantResults.length; i++)
            {
                if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                {
                    onePermissionNotGranted = true;
                }
            }

            if (onePermissionNotGranted)
            {
                Toast.makeText(getApplicationContext() ,"Одно или более необходимых разрешений для приложения не было получено", Toast.LENGTH_SHORT).show();
            }
            RECREATE_ACTIVITY = true;
            this.recreate();
        }
    }

    public boolean isAllRequestedPermissionsGranted(String[] permissions)
    {
        for (String permission : permissions)
        {
            if (checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
            {
                return false;
            }
        }

        return true;
    }
}