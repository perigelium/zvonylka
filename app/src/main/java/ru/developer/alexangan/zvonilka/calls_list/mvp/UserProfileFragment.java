package ru.developer.alexangan.zvonilka.calls_list.mvp;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.storage.Preferences;

import static ru.developer.alexangan.zvonilka.common.Constants.APP_PREFERENCES;

public class UserProfileFragment extends Fragment
{
    @BindView(R.id.tvCallerFullName)
    TextView tvCallerFullName;

    @BindView(R.id.tvCallerFirstNumber)
    TextView tvCallerFirstNumber;

    @BindView(R.id.ivCallerAvatar)
    ImageView ivCallerAvatar;

    @BindView(R.id.llTabCalls)
    LinearLayout llTabCalls;

    @BindView(R.id.tvLogout)
    TextView tvLogout;

    private OnFragmentInteractionListener mListener;

    public UserProfileFragment()
    {
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.user_profile_fragment, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        llTabCalls.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getActivity().getFragmentManager().popBackStack();
                //mListener.openAllContactsFragment();
            }
        });

        tvLogout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Preferences preferences = Preferences.getInstance();
                preferences.setPrefLogin(null);
                preferences.setPrefPassword(null);
                mListener.logout();
            }
        });

        //tvCallerFirstNumber.setText("");
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }
}
