package ru.developer.alexangan.zvonilka.calls_list.mvp;


import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.CallEvent;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.model.Phone;

public class AllContactsFragment extends Fragment implements SearchView.OnQueryTextListener
{
    @BindView(R.id.llGoBack)
    LinearLayout flGoBack;

    @BindView(R.id.rvContacts)
    ListView rvContacts;

    @BindView(R.id.flOpenSearchView)
    FrameLayout flOpenSearchView;

    @BindView(R.id.svContacts)
    SearchView svContacts;

    @BindView(R.id.rvSearchResults)
    ListView rvSearchResults;

    private OnFragmentInteractionListener mListener;
    ArrayList<ContactsBookItem> contacts;
    ArrayList<ContactsBookItem> searchResContacts;

    private ContactsAdapter searchResultsAdapter;

    private String lastSearchString;

    public AllContactsFragment()
    {
    }

    public static AllContactsFragment newInstance(ArrayList<ContactsBookItem> contacts)
    {
        AllContactsFragment f = new AllContactsFragment();
        f.setContacts(contacts);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        searchResContacts = new ArrayList<>();
        lastSearchString = "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.all_contacts_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        flGoBack.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    getActivity().onBackPressed();
                }
                return false;
            }
        });

        flOpenSearchView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openSearchView();
            }
        });

        searchResultsAdapter = new ContactsAdapter(getActivity(), R.layout.list_item_contacts, true);
        rvSearchResults.setAdapter(searchResultsAdapter);

        rvSearchResults.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = searchResContacts.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        final ContactsAdapter mAdapter = new ContactsAdapter(getActivity(), R.layout.list_item_contacts, false);
        rvContacts.setAdapter(mAdapter);

        rvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = contacts.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        if (contacts != null && contacts.size() != 0)
        {
            mAdapter.setItems(contacts);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String queryString)
    {
        mListener.hideSoftKeyboard();
        updateQueriedList(queryString);

        return false;
    }

    private void updateQueriedList(String queryString)
    {
        searchResContacts.clear();
        rvSearchResults.setAdapter(null);

        if (queryString.length() == 0)
        {
            return;
        }

        String strQueryLower = queryString.toLowerCase();

        ArrayList<ContactsBookItem> contactsBookItems = new ArrayList<>();

        for (ContactsBookItem contactsBookItem : contacts)
        {
            String strDisplayName = contactsBookItem.displayName.toLowerCase();

            if (strDisplayName.contains(strQueryLower))
            {
                contactsBookItems.add(contactsBookItem);
                continue;
            }

            for (Phone phone : contactsBookItem.phones)
            {
                String number = phone.number;

                if (number.contains(strQueryLower))
                {
                    contactsBookItems.add(contactsBookItem);
                    break;
                }
            }
        }

        rvSearchResults.setAdapter(searchResultsAdapter);
        searchResContacts.addAll(contactsBookItems);
        searchResultsAdapter.setSearchMatch(strQueryLower);
        searchResultsAdapter.setItems(searchResContacts);
    }

    private void setViewsToDefaultMode()
    {
        svContacts.setOnQueryTextListener(null);
        rvSearchResults.setVisibility(View.GONE);
        svContacts.setVisibility(View.GONE);
        flOpenSearchView.setVisibility(View.VISIBLE);
        rvContacts.setVisibility(View.VISIBLE);
        if (mListener != null)
        {
            mListener.hideSoftKeyboard();
        }
    }

    private void openSearchView()
    {
        svContacts.setOnQueryTextListener(this);
        flOpenSearchView.setVisibility(View.GONE);

        svContacts.setVisibility(View.VISIBLE);
        svContacts.setIconified(false); // opens virtual keyboard
    }

    private void setViewsToSearchMode()
    {
        rvContacts.setVisibility(View.GONE);
        rvSearchResults.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onQueryTextChange(String newText)
    {
            if (newText.length() == 0 && lastSearchString.length() > 0)
            {
                setViewsToDefaultMode();
                return true;
            }

            if (newText.length() == 1 && lastSearchString.length() < 2)
            {
                setViewsToSearchMode();
            }
            updateQueriedList(newText);
            lastSearchString = newText;

            return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof ru.developer.alexangan.zvonilka.calls_list.mvp.OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }

    public void setContacts(ArrayList<ContactsBookItem> contacts)
    {
        this.contacts = contacts;
    }
}
