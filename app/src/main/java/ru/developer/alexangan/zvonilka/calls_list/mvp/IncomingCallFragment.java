package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.utils.Ringer;


public class IncomingCallFragment extends Fragment
{
    @BindView(R.id.tvCallerFullName)
    TextView tvCallerFullName;

    @BindView(R.id.ivCallerAvatar)
    ImageView ivCallerAvatar;

/*    @BindView(R.id.btnSwipeToAnswerCall)
    RelativeLayout btnSwipeToAnswerCall;*/

    @BindView(R.id.btnSwipeToAnswerCall)
    ImageButton btnSwipeToAnswerCall;

    @BindView(R.id.ibPhoneTubeGreen)
    ImageButton ibPhoneTubeGreen;

    @BindView(R.id.ibPhoneTubeRed)
    ImageButton ibPhoneTubeRed;

    private OnFragmentInteractionListener mListener;
    private ContactsBookItem contactsBookItem;

    public IncomingCallFragment()
    {
        // Required empty public constructor
    }

    public static IncomingCallFragment newInstance(String param1, String param2)
    {
        IncomingCallFragment fragment = new IncomingCallFragment();

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.incoming_call_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        ibPhoneTubeGreen.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP)
                {
                    if (!getActivity().isFinishing() && !getActivity().isDestroyed())
                    {
                        getActivity().getFragmentManager().popBackStack();
                        Ringer.getRinger(getActivity()).stopRing();
                        mListener.onIncomingCallAnswered(contactsBookItem);
                    }
                }

                return false;
            }
        });

        ibPhoneTubeRed.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP)
                {
                    if (!getActivity().isFinishing() && !getActivity().isDestroyed())
                    {
                        getActivity().getFragmentManager().popBackStack();
                        mListener.onIncomingCallRejected();
                    }
                }

                return false;
            }
        });

        if (getArguments() != null)
        {
            contactsBookItem = (ContactsBookItem) getArguments().getSerializable("ContactsBookItem");

            if (contactsBookItem != null)
            {
                tvCallerFullName.setText(contactsBookItem.displayName);

                if (contactsBookItem.displayPhotoUri != null)
                {
                    try
                    {
                        ivCallerAvatar.setImageURI(contactsBookItem.displayPhotoUri);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }

        playRingingTone();
    }

    private void playRingingTone()
    {
        Ringer.getRinger(getActivity()).stopRing();
        if (!Ringer.getRinger(getActivity()).isRinging())
        {
            Ringer.getRinger(getActivity()).ring(null, null);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }
}
