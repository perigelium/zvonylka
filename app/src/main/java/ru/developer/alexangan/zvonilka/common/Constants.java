package ru.developer.alexangan.zvonilka.common;

import android.Manifest;

public class Constants
{

    public enum LoginMode
    {
        CREATE, CHECK;
    }

    public static final String EXTRA_MODE = "mode";

    public static final String KEY_INCOMING = "incoming";
    public static final String KEY_NUMBER = "number";
    public static final String APP_PREFERENCES = "mysettings";

    public static final String[] PERMISSION_CONSTANTS =
            { Manifest.permission.RECORD_AUDIO, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CALL_LOG};
}
