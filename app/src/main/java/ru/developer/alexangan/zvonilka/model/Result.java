package ru.developer.alexangan.zvonilka.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class Result implements Parcelable
{
    public String gender;
    public Name name;
    public Location location;
    public String email;
    public Login login;
    public Dob dob;
    public Registered registered;
    public String phone;
    public String cell;
    public Id id;
    public Picture picture;
    public String nat;

    protected Result(Parcel in)
    {
        gender = in.readString();
        email = in.readString();
        phone = in.readString();
        cell = in.readString();
        nat = in.readString();
    }

    public static final Creator<Result> CREATOR = new Creator<Result>()
    {
        @Override
        public Result createFromParcel(Parcel in)
        {
            return new Result(in);
        }

        @Override
        public Result[] newArray(int size)
        {
            return new Result[size];
        }
    };

    @Override
    public int describeContents()
    {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(gender);
        parcel.writeString(email);
        parcel.writeString(phone);
        parcel.writeString(cell);
        parcel.writeString(nat);
    }
}
