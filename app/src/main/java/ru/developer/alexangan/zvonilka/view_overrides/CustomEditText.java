package ru.developer.alexangan.zvonilka.view_overrides;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import ru.developer.alexangan.zvonilka.utils.ViewUtils;

public class CustomEditText extends EditText
{
    Context context;

    public CustomEditText(Context context, AttributeSet attrs)
    {
        super(context, attrs);

        this.context = context;
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event)
    {
        if (keyCode == KeyEvent.KEYCODE_BACK)
        {
            return true;
        }
        return false;
    }
}
