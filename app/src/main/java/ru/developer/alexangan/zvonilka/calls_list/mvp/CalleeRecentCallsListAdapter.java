package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.CallEvent;
import ru.developer.alexangan.zvonilka.utils.MyTextUtils;

public class CalleeRecentCallsListAdapter extends BaseAdapter
{
    private Context mContext;
    private ArrayList<CallEvent> callEvents;
    private int layout_id;

    public CalleeRecentCallsListAdapter(Context context, int layout_id, ArrayList<CallEvent> callEvents)
    {
        this.mContext = context;
        this.callEvents = callEvents;
        this.layout_id = layout_id;
    }

    @Override
    public int getCount()
    {
        return callEvents!= null ? callEvents.size() : 0;
    }

    @Override
    public Object getItem(int i)
    {
        return i;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(layout_id, parent, false);

        TextView tvRecentCallTime = (TextView) row.findViewById(R.id.tvRecentCallTime);
        TextView tvRecentCallNumber = (TextView) row.findViewById(R.id.tvRecentCallNumber);
        TextView tvRecentCallDuration = (TextView) row.findViewById(R.id.tvRecentCallDuration);
        TextView tvRecentCallType = (TextView) row.findViewById(R.id.tvRecentCallType);

        String formattedDateTime = MyTextUtils.toDateTime(callEvents.get(position).dateTime, "dd.MM.yy HH:mm", "");
        tvRecentCallTime.setText(formattedDateTime);

        String strDuration = DateUtils.formatElapsedTime(callEvents.get(position).duration);
        tvRecentCallDuration.setText(strDuration);
        String strCallType = callEvents.get(position).strType;
        tvRecentCallType.setText(strCallType);
        tvRecentCallNumber.setText(callEvents.get(position).number);

        return row;
    }
}
