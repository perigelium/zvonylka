package ru.developer.alexangan.zvonilka.calls_list.mvp;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.utils.ViewUtils;

public class CalltimeNumpadFragment extends Fragment implements View.OnClickListener
{
    @BindView(R.id.tvCallerFullName)
    TextView tvCallerFullName;

    @BindView(R.id.tvCallStatus)
    TextView tvCallStatus;

    @BindView(R.id.ivCallerAvatar)
    ImageView ivCallerAvatar;

    @BindView(R.id.llGoBack)
    LinearLayout llGoBack;


    @BindView(R.id.etTypedDigit)
    EditText etTypedDigit;

    @BindView(R.id.btn0)
    FrameLayout btn0;

    @BindView(R.id.btn1)
    FrameLayout btn1;

    @BindView(R.id.btn2)
    FrameLayout btn2;

    @BindView(R.id.btn3)
    FrameLayout btn3;

    @BindView(R.id.btn4)
    FrameLayout btn4;

    @BindView(R.id.btn5)
    FrameLayout btn5;

    @BindView(R.id.btn6)
    FrameLayout btn6;

    @BindView(R.id.btn7)
    FrameLayout btn7;

    @BindView(R.id.btn8)
    FrameLayout btn8;

    @BindView(R.id.btn9)
    FrameLayout btn9;

    @BindView(R.id.btnAsterisk)
    FrameLayout btnAsterisk;

    @BindView(R.id.btnHash)
    FrameLayout btnHash;

    @BindView(R.id.ibPhoneTubeWhiteInGreen)
    ImageButton ibPhoneTubeWhiteInGreen;

/*    @BindView(R.id.btnBackspace)
    View btnBackspace;*/

    private OnFragmentInteractionListener mListener;
    private ContactsBookItem contactsBookItem;

    public CalltimeNumpadFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.callee_numpad_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        llGoBack.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN)
                {
                    getActivity().onBackPressed();
                }
                return false;
            }
        });

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnAsterisk.setOnClickListener(this);
        btnHash.setOnClickListener(this);

/*        btnBackspace.setOnClickListener(this);

        btnBackspace.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                etTypedDigit.setText("");
                return true;
            }
        });*/

        etTypedDigit.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count)
            {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count)
            {
                String strText = charSequence.toString();

                if (strText.length() != 0)
                {
                    String lastChar = strText.substring(strText.length() - 1);

                    char[] szRes = lastChar.toCharArray(); // Convert String to Char array

                    KeyCharacterMap CharMap = KeyCharacterMap.load(KeyCharacterMap.VIRTUAL_KEYBOARD);

                    KeyEvent[] events = CharMap.getEvents(szRes);

                    int keyCode = events[0].getKeyCode();

                    if(lastChar.equals("*"))
                    {
                        keyCode = KeyEvent.KEYCODE_STAR;
                    }

                    if(lastChar.equals("#"))
                    {
                        keyCode = KeyEvent.KEYCODE_POUND;
                    }

                    mListener.onSendDTFMcode(keyCode);
                }
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

            }
        });

        if (getArguments() != null)
        {
            contactsBookItem = (ContactsBookItem) getArguments().getSerializable("ContactsBookItem");

            if (contactsBookItem != null)
            {
                tvCallerFullName.setText(contactsBookItem.displayName);

                if (contactsBookItem.displayPhotoUri != null)
                {
                    try
                    {
                        ivCallerAvatar.setImageURI(contactsBookItem.displayPhotoUri);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }

    public void setStatusCallClosed()
    {
        ViewUtils.hideSoftKeyboard(getActivity());
        getActivity().getFragmentManager().popBackStack();
    }

    @Override
    public void onClick(View view)
    {
        String strText;

        switch (view.getId())
        {
            case R.id.btn0:
                strText = etTypedDigit.getText() + "0";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn1:
                strText = etTypedDigit.getText() + "1";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn2:
                strText = etTypedDigit.getText() + "2";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn3:
                strText = etTypedDigit.getText() + "3";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn4:
                strText = etTypedDigit.getText() + "4";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn5:
                strText = etTypedDigit.getText() + "5";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn6:
                strText = etTypedDigit.getText() + "6";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn7:
                strText = etTypedDigit.getText() + "7";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn8:
                strText = etTypedDigit.getText() + "8";
                etTypedDigit.setText(strText);
                break;

            case R.id.btn9:
                strText = etTypedDigit.getText() + "9";
                etTypedDigit.setText(strText);
                break;

            case R.id.btnAsterisk:
                strText = etTypedDigit.getText() + "*";
                etTypedDigit.setText(strText);
                break;

            case R.id.btnHash:
                strText = etTypedDigit.getText() + "#";
                etTypedDigit.setText(strText);
                break;

            case R.id.btnBackspace:
                strText = etTypedDigit.getText().toString();
                etTypedDigit.setText(strText.substring(0, strText.length() - 1));
                break;
        }
    }
}
