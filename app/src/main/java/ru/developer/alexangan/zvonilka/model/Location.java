
package ru.developer.alexangan.zvonilka.model;


public class Location {

    public String street;
    public String city;
    public String state;
    public String postcode;
    public Coordinates coordinates;
    public Timezone timezone;

}
