package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.ArrayMap;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;

import static ru.developer.alexangan.zvonilka.common.Constants.APP_PREFERENCES;

public class ContactsAdapter extends BaseAdapter
{
    private Context mContext;
    private int layout_id;
    private List<ContactsBookItem> resultList = new ArrayList<>();
    private String searchMatch;
    private boolean searchMode;
    private String lastFirstLetter;
    private ArrayList<String> firstLetters;

    public ContactsAdapter(Context context, int layout_id, boolean searchMode)
    {
        this.mContext = context;
        this.layout_id = layout_id;
        this.searchMode = searchMode;
        firstLetters = new ArrayList<>();
    }

    public void setItems(ArrayList<ContactsBookItem> results)
    {
        resultList = results;
        notifyDataSetChanged();
    }

    public void setSearchMatch(String searchMatch)
    {
        this.searchMatch = searchMatch;
    }

    @Override
    public int getCount()
    {
        return resultList != null ? resultList.size() : 0;
    }

    @Override
    public Object getItem(int i)
    {
        return resultList.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(layout_id, parent, false);

        TextView tvContactName = row.findViewById(R.id.tvContactName);
        TextView tvFirstLetter = row.findViewById(R.id.tvFirstLetter);
        TextView tvFirstLetterAvatar = row.findViewById(R.id.tvFirstLetterAvatar);
        ImageView ivListItemAvatar = row.findViewById(R.id.ivListItemAvatar);

        ContactsBookItem contact = resultList.get(position);
        tvContactName.setText(contact.displayName);

        String nameFirstLetter = contact.displayName.substring(0, 1).toUpperCase();

        if (contact.displayPhotoUri != null)
        {
            try
            {
                ivListItemAvatar.setImageURI(contact.displayPhotoUri);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        } else if (contact.thumbPhotoUri != null)
        {
            try
            {
                ivListItemAvatar.setImageURI(contact.thumbPhotoUri);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        } else
        {
            tvFirstLetterAvatar.setText(nameFirstLetter);
        }

        tvFirstLetter.setText(contact.nameFirstLetter);

/*        SharedPreferences mSettings = mContext.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        String searchString = mSettings.getString("SearchLastQueryString", "");*/

        if (searchMode && searchMatch != null)
        {
            String strNameLower = contact.displayName.toLowerCase();

            if (strNameLower.length() != 0 && strNameLower.contains(searchMatch))
            {
                SpannableString ssText = makeColoredString(contact.displayName, searchMatch);
                tvContactName.setText(ssText);
            }
        }
        return row;
    }

    @Override
    public int getItemViewType(int position)
    {
        return super.getItemViewType(position);
    }

    @NonNull
    private SpannableString makeColoredString(String strSource, String searchString)
    {
        String sourceStrLower = strSource.toLowerCase();

        int iStart = sourceStrLower.indexOf(searchString);
        int iEnd = iStart + searchString.length();

        Spannable spannable = new SpannableString(strSource);
        SpannableString ssText = new SpannableString(spannable);
        ssText.setSpan(new ForegroundColorSpan(Color.parseColor("#FFC3F3")), iStart, iEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ssText;
    }

/*    private class ViewHolder
    {
        public TextView tvContactName;
        public ImageView ivListItemAvatar;

        public ViewHolder(View itemView)
        {
            tvContactName = itemView.findViewById(R.id.tvContactName);
            ivListItemAvatar = itemView.findViewById(R.id.ivListItemAvatar);
        }
    }*/
}