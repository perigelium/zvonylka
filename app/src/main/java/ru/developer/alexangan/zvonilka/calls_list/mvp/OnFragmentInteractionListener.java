package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.view.View;
import android.widget.SearchView;

import java.util.ArrayList;

import ru.developer.alexangan.zvonilka.model.ContactsBookItem;

public interface OnFragmentInteractionListener
{
    void openMainFragment();
    void openAllContactsFragment(ArrayList<ContactsBookItem> contacts);
    void openAllRecentCallsFragment(ArrayList<ContactsBookItem> contacts);
    void openOutgoingCallFragment(ContactsBookItem contactsBookItem);
    void openIncomingCallFragment(ContactsBookItem contactsBookItem);
    void openCalleeFragment(ContactsBookItem contactsBookItem);

    void onIncomingCallAnswered(ContactsBookItem contactsBookItem);
    void onIncomingCallRejected();
    void onUserCallEnvoked(String callee);
    void onUserHangUp();
    void onUserMuteMicrofon(boolean mute);
    void onUserHoldCall(boolean hold);

    void openNewContactFragment(ArrayList<ContactsBookItem> contacts);

    void onSendDTFMcode(int code);

    void openCalltimeNumpadFragment(ContactsBookItem contactsBookItem);

    void openUserProfileFragment();

    void hideSoftKeyboard();

    void logout();
}
