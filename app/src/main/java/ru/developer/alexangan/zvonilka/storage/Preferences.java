package ru.developer.alexangan.zvonilka.storage;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences
{
    private static volatile Preferences instance;
    private static final String className = Preferences.class.getSimpleName();

    private Preferences()
    {
    }

    public static Preferences getInstance()
    {
        if (instance == null)
        {
            synchronized (Preferences.class)
            {
                if (instance == null)
                {
                    instance = new Preferences();
                }
            }
        }
        return instance;
    }

    final static String FILE_NAME = "preferences";

    final static String PREF_LOGIN = "login";
    final static String PREF_PASSWORD = "password";

    private SharedPreferences preferences;

    public void init(Context context)
    {
        preferences = context.getApplicationContext().getSharedPreferences(FILE_NAME, 0);
    }

    private SharedPreferences.Editor getEditor()
    {
        return preferences.edit();
    }

    public void setPrefLogin(String data)
    {
        getEditor().putString(PREF_LOGIN, data).commit();
    }
    public void setPrefPassword(String data)
    {
        getEditor().putString(PREF_PASSWORD, data).commit();
    }

    public String getLogin()
    {
        return preferences.getString(PREF_LOGIN, "");
    }

    public String getPrefPassword()
    {
        return preferences.getString(PREF_PASSWORD, "");
    }
}
