package ru.developer.alexangan.zvonilka.model;

public class CallEvent
{
    public String number;
    public long dateTime;
    public int type; // 1-7
    public String strType;
    public long duration; // seconds

    public CallEvent(String number, long dateTime, int type, String strType, long duration)
    {
        this.number = number;
        this.dateTime = dateTime;
        this.type = type;
        this.strType = strType;
        this.duration = duration;
    }
}
