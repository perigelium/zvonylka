package ru.developer.alexangan.zvonilka.calls_list.mvp;


import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.model.Phone;

public class AllRecentCallsFragment extends Fragment implements SearchView.OnQueryTextListener
{
    @BindView(R.id.llGoBack)
    LinearLayout flGoBack;

    @BindView(R.id.lvRecentCalls)
    ListView rvRecentCalls;

    @BindView(R.id.flOpenSearchView)
    FrameLayout flOpenSearchView;

    @BindView(R.id.svContacts)
    SearchView svContacts;

    @BindView(R.id.rvSearchResults)
    ListView rvSearchResults;

    private OnFragmentInteractionListener mListener;
    ArrayList<ContactsBookItem> recentContacts;
    ArrayList<ContactsBookItem> searchResContacts;

    private RecentCallsAdapter searchResultsAdapter;

    private String lastSearchString;

    public AllRecentCallsFragment()
    {
    }

    public static AllRecentCallsFragment newInstance(ArrayList<ContactsBookItem> contacts)
    {
        AllRecentCallsFragment f = new AllRecentCallsFragment();
        f.setContacts(contacts);
        return f;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        searchResContacts = new ArrayList<>();
        lastSearchString = "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.all_recent_calls_fragment, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        flGoBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getActivity().onBackPressed();
            }
        });

        flOpenSearchView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                openSearchView();
            }
        });

        searchResultsAdapter = new RecentCallsAdapter(getActivity(), R.layout.list_item_recent_calls, true);
        rvSearchResults.setAdapter(searchResultsAdapter);

        rvSearchResults.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = searchResContacts.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        final RecentCallsAdapter mRecentsAdapter = new RecentCallsAdapter(getActivity(), R.layout.list_item_recent_calls, false);
        rvRecentCalls.setAdapter(mRecentsAdapter);

        rvRecentCalls.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = recentContacts.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        if (recentContacts!= null && recentContacts.size() != 0)
        {
            mRecentsAdapter.setItems(recentContacts);
        }
    }

    @Override
    public boolean onQueryTextSubmit(String queryString)
    {
        mListener.hideSoftKeyboard();
        updateQueriedList(queryString);

        return false;
    }

    private void updateQueriedList(String queryString)
    {
        searchResContacts.clear();
        rvSearchResults.setAdapter(null);

        if (queryString.length() == 0)
        {
            return;
        }

        String strQueryLower = queryString.toLowerCase();

        ArrayList<ContactsBookItem> contactsBookItems = new ArrayList<>();

        for (ContactsBookItem contactsBookItem : recentContacts)
        {
            String strDisplayName = contactsBookItem.displayName.toLowerCase();

            if (strDisplayName.contains(strQueryLower))
            {
                contactsBookItems.add(contactsBookItem);
                continue;
            }

            for (Phone phone : contactsBookItem.phones)
            {
                String number = phone.number;

                if (number.contains(strQueryLower))
                {
                    contactsBookItems.add(contactsBookItem);
                    break;
                }
            }
        }

        rvSearchResults.setAdapter(searchResultsAdapter);
        searchResContacts.addAll(contactsBookItems);
        searchResultsAdapter.setSearchMatch(strQueryLower);
        searchResultsAdapter.setItems(searchResContacts);
    }

    private void setViewsToDefaultMode()
    {
        svContacts.setOnQueryTextListener(null);
        rvSearchResults.setVisibility(View.GONE);
        svContacts.setVisibility(View.GONE);
        flOpenSearchView.setVisibility(View.VISIBLE);
        rvRecentCalls.setVisibility(View.VISIBLE);
        if (mListener != null)
        {
            mListener.hideSoftKeyboard();
        }
    }

    private void openSearchView()
    {
        svContacts.setOnQueryTextListener(this);
        flOpenSearchView.setVisibility(View.GONE);

        svContacts.setVisibility(View.VISIBLE);
        svContacts.setIconified(false); // opens virtual keyboard
    }

    private void setViewsToSearchMode()
    {
        rvRecentCalls.setVisibility(View.GONE);
        rvSearchResults.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onQueryTextChange(String newText)
    {
        if (newText.length() == 0 && lastSearchString.length() > 0)
        {
            setViewsToDefaultMode();
            return true;
        }

        if (newText.length() == 1 && lastSearchString.length() < 2)
        {
            setViewsToSearchMode();
        }
        updateQueriedList(newText);
        lastSearchString = newText;

        return true;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof ru.developer.alexangan.zvonilka.calls_list.mvp.OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }

    public void setContacts(ArrayList<ContactsBookItem> contacts)
    {
        recentContacts = new ArrayList<>();

        if (contacts.size() != 0)
        {
            for (ContactsBookItem contactsBookItem : contacts)
            {
                if (contactsBookItem.calls != null && contactsBookItem.calls.size() != 0)
                {
                    recentContacts.add(contactsBookItem);
                }
            }
        }
    }
}
