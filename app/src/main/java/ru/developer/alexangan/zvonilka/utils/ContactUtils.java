package ru.developer.alexangan.zvonilka.utils;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.res.AssetFileDescriptor;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Pair;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ru.developer.alexangan.zvonilka.model.ContactsBookItem;

public class ContactUtils
{
    private static volatile ContactUtils instance;
    private static final String className = ContactUtils.class.getSimpleName();

    private ContactUtils()
    {
    }

    public static ContactUtils getInstance()
    {
        if (instance == null)
        {
            synchronized (ContactUtils.class)
            {
                if (instance == null)
                {
                    instance = new ContactUtils();
                }
            }
        }
        return instance;
    }

    public String getDisplayName(Context context, long id)
    {
        String displayName = null;
        // define the columns I want the query to return
        final String[] projection = new String[]{ContactsContract.Contacts.DISPLAY_NAME,};
        final Cursor cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id + ""}, null);
        if (cursor != null)
        {
            if (cursor.moveToFirst())
            {
                displayName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }
            cursor.close();
        }
        return displayName;
    }

    public ArrayList<String> getContactIdByPhoneNumber(Context ctx, String phoneNumber)
    {
        String contactId = null;
        Cursor cursor = null;
        ArrayList<String> contactsIds = new ArrayList<>();

        if (phoneNumber != null && phoneNumber.length() > 0)
        {
            ContentResolver contentResolver = ctx.getContentResolver();

            Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));

            //Uri uri = Uri.withAppendedPath(ContactsContract.CommonDataKinds.Phone.CONTENT_FILTER_URI, Uri.encode(partial));
            //Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);

            String[] projection = new String[]{ContactsContract.PhoneLookup._ID};
            String[] projectionFields = new String[]{ContactsContract.Contacts._ID, ContactsContract.CommonDataKinds.Phone.NUMBER};

            cursor = contentResolver.query(uri, projection, null, null, null);

            if (cursor != null)
            {
                while (cursor.moveToNext())
                {
                    contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                    contactsIds.add(contactId);
                }
                cursor.close();
            }
        }
        return contactsIds;
    }

    public String getContactNameByPhoneNumber(Context ctx, String phoneNumber)
    {
        String displayName = null;
        Uri lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phoneNumber));
        Cursor c = ctx.getContentResolver().query(lookupUri, new String[]{ContactsContract.Data.DISPLAY_NAME}, null, null, null);
        try
        {
            if (c.moveToFirst())
            {
                displayName = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            }
        } catch (Exception e)
        {
            e.printStackTrace();
        } finally
        {
            c.close();
        }
        return displayName;
    }

    public Uri getThumbPhotoUri(long contactId, ContentResolver cResolver)
    {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri photoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.CONTENT_DIRECTORY);

        Cursor cursor = cResolver.query(photoUri, new String[]{ContactsContract.Contacts.Photo.PHOTO}, null, null, null);

        if (cursor == null)
        {
            return null;
        }
        try
        {
            if (cursor.moveToFirst())
            {
                byte[] data = cursor.getBlob(0);
                if (data != null)
                {
                    return photoUri; //new ByteArrayInputStream(data);
                }
            }
        } finally
        {
            cursor.close();
        }

        return null;
    }

    public Uri getDisplayPhotoUri(long contactId, ContentResolver cResolver)
    {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);
        try
        {
            AssetFileDescriptor fd = cResolver.openAssetFileDescriptor(displayPhotoUri, "r");
            return displayPhotoUri;// fd.createInputStream();
        } catch (IOException e)
        {
            return null;
        }
    }

/*    public int getCountryCodeLength(String phone)
    {
        PhoneNumberUtil phoneInstance = PhoneNumberUtil.getInstance();
        try
        {
            Phonenumber.PhoneNumber phoneNumber = phoneInstance.parse(phone, null);
            //long nationalNumber = phoneNumber.getNationalNumber();
            int countryCode = phoneNumber.getCountryCode();
            return String.valueOf(countryCode).length();
        } catch (Exception e)
        {
        }
        return phone.length();
    }*/

/*    public Pair<Uri, Uri> getImagesByContactId(String contactId, ContentResolver cr)
    {
        Uri thumbUri = null;
        Uri imageUri = null;

        if (contactId != null)
        {
            String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone.PHOTO_URI, ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI};
            Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, projection, ContactsContract.CommonDataKinds.Phone._ID + " = ?", new String[]{contactId}, null);
            String imageUriString = null;
            String thumbnailUriString = null;

            if (cur.moveToFirst())
            {
                imageUriString = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));
                thumbnailUriString = cur.getString(cur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI));
            }
            thumbUri = thumbnailUriString != null ? Uri.parse(thumbnailUriString) : null;
            imageUri = imageUriString != null ? Uri.parse(imageUriString) : null;
        }
        Pair<Uri, Uri> res = new Pair<>(thumbUri, imageUri);
        return res;
    }*/
}
