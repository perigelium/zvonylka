package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;

import com.avoapp.avosdk.AvoSipListener;
import com.avoapp.avosdk.AvoSipPort;

import java.util.ArrayList;

import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.StartActivity;
import ru.developer.alexangan.zvonilka.api.RandomUsersApi;
import ru.developer.alexangan.zvonilka.avosdk.Constants;
import ru.developer.alexangan.zvonilka.avosdk.Event;
import ru.developer.alexangan.zvonilka.login.mvp.LoginActivity;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.model.Phone;
import ru.developer.alexangan.zvonilka.utils.ContactUtils;
import ru.developer.alexangan.zvonilka.utils.ViewUtils;


public class CallListActivity extends Activity implements OnFragmentInteractionListener, CallListContract.View, AvoSipListener
{
    private UsersPresenter presenter;
    private ProgressDialog progressDialog;
    RandomUsersApi randomUsersApi;
    private FragmentManager mFragmentManager;
    private AvoSipPort mAvoSipPort;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ViewUtils.setStatusAndNavBarTransparent(this);

        setContentView(R.layout.call_list_activity);

        ButterKnife.bind(this);

        init();

        openMainFragment();

/*        final List<ContactsBookItem> contactsBookItems = getContactBookList();

        ContactUserAdapter mAdapter = new ContactUserAdapter(CallListActivity.this, new ContactUserAdapter.ClickCallback()
        {
            @Override
            public void onItemClick(int position)
            {
                ContactsBookItem contactsBookItem = contactsBookItems.get(position);
                openCalleeFragment(contactsBookItem);
            }
        });

        if (contactsBookItems != null && contactsBookItems.size() != 0)
        {
            mAdapter.setItems(contactsBookItems);
            recyclerView.setAdapter(mAdapter);
        }*/
    }

/*    private void initViews()
    {
        recyclerView = findViewById(R.id.call_list_all);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }*/

    private void init()
    {
/*        DbHelper dbHelper = new DbHelper(this);
        UsersModel usersModel = new UsersModel(dbHelper);
        presenter = new UsersPresenter(usersModel);
        presenter.attachView(this);
        presenter.viewIsReady();

        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        File cacheFile = new File(this.getCacheDir(), "HttpCache");
        cacheFile.mkdirs();

        Cache cache = new Cache(cacheFile, 10 * 1000 * 1000); //10 MB

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder().cache(cache).build();

        Retrofit retrofit = new Retrofit.Builder().client(okHttpClient).baseUrl("https://randomuser.me/").addConverterFactory(GsonConverterFactory.create(gson)).build();
        randomUsersApi = retrofit.create(RandomUsersApi.class);*/

        updateParamsFromServer();
    }

    public void showProgress()
    {
        progressDialog = ProgressDialog.show(this, "", getString(R.string.please_wait));
    }

    public void hideProgress()
    {
        if (progressDialog != null)
        {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showMessage(int messageResId)
    {

    }

/*    private List<ContactsBookItem> getContactBookList()
    {
        ContentResolver cr = this.getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        //Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, "DISPLAY_NAME = '" + NAME + "'", null, null);

        ArrayList<ContactsBookItem> contactsBookItems = new ArrayList<>();

        int contactsCount = cur.getCount();

        if (contactsCount > 0)
        {
            while (cur.moveToNext())
            {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                String displayName = cur.getString(cur.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (Integer.parseInt(cur.getString(cur.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
                {
                    Uri photoUri = getPhotoUri(id);

                    Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + id, null, null);

                    ArrayList<String> phoneNumbers = new ArrayList<>();

                    while (phones.moveToNext())
                    {
                        String number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                        //int type = phones.getInt(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));

                        phoneNumbers.add(number);
                    }
                    phones.close();

                    ContactsBookItem contactsBookItem = new ContactsBookItem(displayName, photoUri, phoneNumbers);

                    contactsBookItems.add(contactsBookItem);
                }

            }
        }
        cur.close();
        return contactsBookItems;
    }*/

    public void replaceFragment(Fragment newFragment, boolean addToBack, boolean clearBackStack)
    {
        mFragmentManager = getFragmentManager();

        FragmentTransaction mFragmentTransaction = mFragmentManager.beginTransaction();

        mFragmentTransaction.replace(R.id.fragContainer, newFragment, newFragment.getClass().getName());

        if (clearBackStack)
        {
            mFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        if (addToBack)
        {
            mFragmentTransaction.addToBackStack(newFragment.getClass().getName());
        }

        try
        {
            mFragmentTransaction.commit();//commitAllowingStateLoss();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void openAllContactsFragment(ArrayList<ContactsBookItem> contacts)
    {
/*        Bundle bundle = new Bundle();
        bundle.put("Contacts", (ArrayList<? extends Parcelable>) contacts);*/
        Fragment fragment = AllContactsFragment.newInstance(contacts);
        //fragment.setArguments(bundle);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void openAllRecentCallsFragment(ArrayList<ContactsBookItem> contacts)
    {
/*        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("Contacts", (ArrayList<? extends Parcelable>) contacts);*/
        Fragment fragment = AllRecentCallsFragment.newInstance(contacts);
        //fragment.setArguments(bundle);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void openMainFragment()
    {
/*        Bundle bundle = new Bundle();
        bundle.putSerializable("ContactsBookItem", contactsBookItem);*/
        Fragment fragment = new MainFragment();
        //fragment.setArguments(bundle);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void openOutgoingCallFragment(ContactsBookItem contactsBookItem)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("ContactsBookItem", contactsBookItem);
        Fragment fragment = new OutgoingCallFragment();
        fragment.setArguments(bundle);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void openIncomingCallFragment(ContactsBookItem contactsBookItem)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("ContactsBookItem", contactsBookItem);
        Fragment fragment = new IncomingCallFragment();
        fragment.setArguments(bundle);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void openCalleeFragment(ContactsBookItem contactsBookItem)
    {
        ViewUtils.hideSoftKeyboard(this);
        Bundle bundle = new Bundle();
        bundle.putSerializable("ContactsBookItem", contactsBookItem);
        Fragment fragment = new CalleeFragment();
        fragment.setArguments(bundle);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void openCalltimeNumpadFragment(ContactsBookItem contactsBookItem)
    {
        Bundle bundle = new Bundle();
        bundle.putSerializable("ContactsBookItem", contactsBookItem);
        Fragment fragment = new CalltimeNumpadFragment();
        fragment.setArguments(bundle);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void openUserProfileFragment()
    {
        Fragment fragment = new UserProfileFragment();
        replaceFragment(fragment, true, false);
    }

    @Override
    public void hideSoftKeyboard()
    {
        ViewUtils.hideSoftKeyboard(this);
    }

    @Override
    public void logout()
    {
        this.finish();
        Intent intent = new Intent(CallListActivity.this, StartActivity.class);
        startActivity(intent);
    }

    @Override
    public void onIncomingCallAnswered(ContactsBookItem contactsBookItem)
    {
        openOutgoingCallFragment(contactsBookItem);
        mAvoSipPort.answerCall();
    }

    @Override
    public void onIncomingCallRejected()
    {
        Log.d("DEBUG", "onIncomingCallRejected");
        mAvoSipPort.rejectCall();
    }

    @Override
    public void onSendDTFMcode(int code)
    {
        mAvoSipPort.sendDtmfCode(code);
    }

    @Override
    public void onUserCallEnvoked(String callee)
    {
        Log.d("DEBUG", "onUserCallEnvoked");
        mAvoSipPort.call(callee);
    }

    @Override
    public void onUserHangUp()
    {
        Log.d("DEBUG", "onHangUpEnvoked");
        mAvoSipPort.hangUp();

        if (!this.isFinishing() && !this.isDestroyed() && mFragmentManager.getBackStackEntryCount() > 1)
        {
            onBackPressed();
        }
    }

    @Override
    public void onUserMuteMicrofon(boolean mute)
    {
        mAvoSipPort.muteMicrophone(mute);
        OutgoingCallFragment fragment = (OutgoingCallFragment) mFragmentManager.findFragmentByTag(OutgoingCallFragment.class.getName());
        if (fragment != null)
        {
            fragment.setMicrofonStatus(mute);
        }
    }

    @Override
    public void onUserHoldCall(boolean hold)
    {
        if (hold)
        {
            mAvoSipPort.hold();
        } else
        {
            mAvoSipPort.unhold();
        }

        OutgoingCallFragment fragment = (OutgoingCallFragment) mFragmentManager.findFragmentByTag(OutgoingCallFragment.class.getName());
        if (fragment != null)
        {
            fragment.setHoldStatus(hold);
        }
    }

    @Override
    public void openNewContactFragment(ArrayList<ContactsBookItem> contacts)
    {
        Fragment fragment = NewContactFragment.newInstance(contacts);

        replaceFragment(fragment, true, false);
    }

    @Override
    public void onRegisterSuccess()
    {
        Log.d("DEBUG", "on sip Register Success");
    }

    @Override
    public void onRegisterFailure(int i)
    {
        Log.d("DEBUG", "on sip Register Failure");
    }

    @Override
    public void onInviteIncoming(String caller)
    {
        Log.d("DEBUG", "onInviteIncoming");
        ArrayList<Phone> phones = new ArrayList<>();
        phones.add(new Phone(caller, 0));
        ContactUtils contactUtils = ContactUtils.getInstance();
        String calleeName = contactUtils.getContactNameByPhoneNumber(this, caller);
        if (calleeName == null)
        {
            calleeName = caller;
        }
        ContactsBookItem contactsBookItem = new ContactsBookItem(calleeName, phones, null, null);
        openIncomingCallFragment(contactsBookItem);
    }

    @Override
    public void onInviteRinging()
    {
        Log.d("DEBUG", "on Invite Ringing");
        OutgoingCallFragment fragment = (OutgoingCallFragment) mFragmentManager.findFragmentByTag(OutgoingCallFragment.class.getName());
        if (fragment != null)
        {
            fragment.setStatusRinging();
        }
    }

    @Override
    public void onInviteEarly()
    {
        Log.d("DEBUG", "onInviteEarly");
    }

    @Override
    public void onInviteConnecting()
    {
        Log.d("DEBUG", "onInviteConnecting");
        OutgoingCallFragment fragment = (OutgoingCallFragment) mFragmentManager.findFragmentByTag(OutgoingCallFragment.class.getName());
        if (fragment != null)
        {
            fragment.setStatusConnecting();
        }
    }

    @Override
    public void onInviteAnswered()
    {
        Log.d("DEBUG", "onInviteAnswered");
        OutgoingCallFragment fragment = (OutgoingCallFragment) mFragmentManager.findFragmentByTag(OutgoingCallFragment.class.getName());
        if (fragment != null)
        {
            fragment.setStatusAnswered();
        }
    }

    @Override
    public void onInviteRejected()
    {
        Log.d("DEBUG", "onInviteRejected");

        if (!this.isFinishing() && !this.isDestroyed() && mFragmentManager.getBackStackEntryCount() > 1)
        {
            onBackPressed();
        }
    }

    @Override
    public void onCallClosed()
    {
        Log.d("DEBUG", "onCallClosed");

        OutgoingCallFragment outgoingCallFragment = (OutgoingCallFragment) mFragmentManager.findFragmentByTag(OutgoingCallFragment.class.getName());
        if (outgoingCallFragment != null)
        {
            outgoingCallFragment.setStatusCallClosed();
        }

        CalltimeNumpadFragment calltimeNumpadFragment = (CalltimeNumpadFragment) mFragmentManager.findFragmentByTag(CalltimeNumpadFragment.class.getName());
        if (calltimeNumpadFragment != null)
        {
            calltimeNumpadFragment.setStatusCallClosed();
        }

        if (!this.isFinishing() && !this.isDestroyed() && mFragmentManager.getBackStackEntryCount() > 1)
        {
            onBackPressed();
        }
    }

    private void initAvoSipPort()
    {
        if (mAvoSipPort == null)
        {
            mAvoSipPort = new AvoSipPort(this);
        }

        mAvoSipPort.setUser(Constants.SIP_USER, Constants.SIP_PASS, Constants.LOCAL_SIP_PORT, Constants.SIP_SERVER, Constants.SIP_PORT, Constants.STUN_SERVER, Constants.STUN_PORT, Constants.TURN_SERVER, Constants.TURN_PORT, Constants.TURN_USER, Constants.TURN_PASS);
    }

    private void updateParamsFromServer()
    {
        initAvoSipPort();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (mAvoSipPort != null)
        {
            mAvoSipPort.destroy();
        }
    }

    public void onCallActionRequested(Event event)
    {
        int eventId = event.getEventId();
        if (eventId == Event.EVENT_HANG_UP)
        {
            mAvoSipPort.hangUp();
        } else if (eventId == Event.EVENT_ACCEPT)
        {
            mAvoSipPort.answerCall();
        } else if (eventId == Event.EVENT_DECLINE)
        {
            mAvoSipPort.rejectCall();
        } else if (eventId == Event.EVENT_HOLD)
        {
            mAvoSipPort.hold();
        } else if (eventId == Event.EVENT_UNHOLD)
        {
            mAvoSipPort.unhold();
        } else if (eventId == Event.EVENT_MUTE)
        {
            mAvoSipPort.muteMicrophone(true);
        } else if (eventId == Event.EVENT_UNMUTE)
        {
            mAvoSipPort.muteMicrophone(false);
        }
    }

    @Override
    public void onBackPressed()
    {
        if (mFragmentManager.getBackStackEntryCount() == 1)
        {
            finish();
        }

/*        final NewContactFragment fragment = (NewContactFragment) getFragmentManager().findFragmentByTag(NewContactFragment.class.getName());

        if (fragment!=null && fragment.allowBackPressed)
        { // and then you define a method allowBackPressed with the logic to allow back pressed or not
            super.onBackPressed();
        }*/

        super.onBackPressed();
    }
}
