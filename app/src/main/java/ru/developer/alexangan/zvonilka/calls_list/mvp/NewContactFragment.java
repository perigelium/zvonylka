package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.model.Phone;
import ru.developer.alexangan.zvonilka.utils.ViewUtils;


public class NewContactFragment extends Fragment implements View.OnClickListener
{
    @BindView(R.id.flCreateContact)
    LinearLayout flCreateContact;

    @BindView(R.id.etNumber)
    EditText etTypedPhoneNumber;

    @BindView(R.id.lvExistingContactsByPhoneNumber)
    ListView lvExistingContactsByPhoneNumber;

    @BindView(R.id.ibPhoneTubeWhiteInGreen)
    ImageButton ibPhoneTubeWhiteInGreen;

    @BindView(R.id.btn0)
    FrameLayout btn0;

    @BindView(R.id.btn1)
    FrameLayout btn1;

    @BindView(R.id.btn2)
    FrameLayout btn2;

    @BindView(R.id.btn3)
    FrameLayout btn3;

    @BindView(R.id.btn4)
    FrameLayout btn4;

    @BindView(R.id.btn5)
    FrameLayout btn5;

    @BindView(R.id.btn6)
    FrameLayout btn6;

    @BindView(R.id.btn7)
    FrameLayout btn7;

    @BindView(R.id.btn8)
    FrameLayout btn8;

    @BindView(R.id.btn9)
    FrameLayout btn9;

    @BindView(R.id.btnAsterisk)
    FrameLayout btnAsterisk;

    @BindView(R.id.btnHash)
    FrameLayout btnHash;

    @BindView(R.id.btnBackspace)
    ImageButton btnBackspace;

    ArrayList<ContactsBookItem> contacts;
    private OnFragmentInteractionListener mListener;
    public boolean allowBackPressed;
    ArrayList<ContactsBookItem> alSearchResults;
    private SearchByPhoneListAdapter mAdapter;

    public NewContactFragment()
    {
    }

    public static NewContactFragment newInstance(ArrayList<ContactsBookItem> contacts)
    {
        NewContactFragment f = new NewContactFragment();
        f.setContacts(contacts);
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.new_contact_fragment, container, false);
        ButterKnife.bind(this, view);
        initViews(view);
        return view;
    }

    private void initViews(View view)
    {

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        alSearchResults = new ArrayList<>();

        allowBackPressed = false;

        btn0.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        btn5.setOnClickListener(this);
        btn6.setOnClickListener(this);
        btn7.setOnClickListener(this);
        btn8.setOnClickListener(this);
        btn9.setOnClickListener(this);
        btnAsterisk.setOnClickListener(this);
        btnHash.setOnClickListener(this);
        btnBackspace.setOnClickListener(this);

        btnBackspace.setVisibility(View.VISIBLE);

        btnBackspace.setOnLongClickListener(new View.OnLongClickListener()
        {
            @Override
            public boolean onLongClick(View view)
            {
                etTypedPhoneNumber.setText("");
                return true;
            }
        });


        lvExistingContactsByPhoneNumber.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = alSearchResults.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        mAdapter = new SearchByPhoneListAdapter(getActivity(), R.layout.list_item_search_dialer, alSearchResults);
        lvExistingContactsByPhoneNumber.setAdapter(mAdapter);

/*        etTypedPhoneNumber.requestFocus();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        if (imm != null)
        {
            imm.showSoftInput(etTypedPhoneNumber, InputMethodManager.SHOW_IMPLICIT);
        }*/

/*        etTypedPhoneNumber.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                //if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    Log.i("DEBUG", "IME_ACTION_DONE");
                    return true;
                }
                //return false;
            }
        });

        etNewContactName.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event)
            {
                //if (actionId == EditorInfo.IME_ACTION_DONE)
                {
                    Log.i("DEBUG", "IME_ACTION_DONE");
                    return true;
                }
                //return false;
            }
        });*/

        etTypedPhoneNumber.addTextChangedListener(new TextWatcher()
        {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int start, int before, int count)
            {
                lvExistingContactsByPhoneNumber.setAdapter(null);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count)
            {
                int strLen = charSequence.toString().length();
                if (charSequence.toString().startsWith("+"))
                {
                    strLen += 1;
                    //countryPrefixLength = ContactUtils.getInstance().getCountryCodeLength(charSequence.toString());
                }

                if (strLen > 2)
                {
                    alSearchResults = findContactByPartPhoneNumber(charSequence.toString());
                    mAdapter.setItems(alSearchResults);
                    lvExistingContactsByPhoneNumber.setAdapter(mAdapter);
                }
            }

            @Override
            public void afterTextChanged(Editable editable)
            {

            }
        });

        ibPhoneTubeWhiteInGreen.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP)
                {
                    if (!getActivity().isFinishing() && !getActivity().isDestroyed())
                    {
                        String callee = etTypedPhoneNumber.getText().toString();

                        if (callee.length() > 2)
                        {
                            ViewUtils.hideSoftKeyboard(getActivity());
                            getActivity().getFragmentManager().popBackStack();

                            ArrayList<Phone> phones = new ArrayList<Phone>();
                            phones.add(new Phone(callee, 0));
                            mListener.openOutgoingCallFragment(new ContactsBookItem(null, phones, null, null));
                            mListener.onUserCallEnvoked(callee);
                        } else
                        {
                            Toast.makeText(getActivity(), "Заполните поле номера телефона, пожалуйста", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                return true;
            }
        });

        flCreateContact.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!getActivity().isFinishing() && !getActivity().isDestroyed())
                {
                    String phoneNumber = etTypedPhoneNumber.getText().toString();

                    Intent intent = new Intent(Intent.ACTION_INSERT);
                    intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
                    intent.putExtra(ContactsContract.Intents.Insert.PHONE, phoneNumber);

                    startActivityForResult(intent, 100);
                }
            }
        });
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }

    public void setContacts(ArrayList<ContactsBookItem> contacts)
    {
        this.contacts = contacts;
    }

    public ArrayList<ContactsBookItem> findContactByPartPhoneNumber(String match)
    {
        alSearchResults = new ArrayList<>();

        for (ContactsBookItem contactsBookItem : contacts)
        {
            if(contactsBookItem.displayName.contains(match))
            {
                alSearchResults.add(contactsBookItem);
            }
            else
            {
                for(Phone phone : contactsBookItem.phones)
                {
                    String strNumber = phone.number;
                    if (strNumber.contains(match))
                    {
                        alSearchResults.add(contactsBookItem);
                        break;
                    }
                }
            }
        }
        return alSearchResults;
    }

    @Override
    public void onClick(View view)
    {
        String strText;

        switch (view.getId())
        {
            case R.id.btn0:
                strText = etTypedPhoneNumber.getText() + "0";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn1:
                strText = etTypedPhoneNumber.getText() + "1";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn2:
                strText = etTypedPhoneNumber.getText() + "2";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn3:
                strText = etTypedPhoneNumber.getText() + "3";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn4:
                strText = etTypedPhoneNumber.getText() + "4";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn5:
                strText = etTypedPhoneNumber.getText() + "5";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn6:
                strText = etTypedPhoneNumber.getText() + "6";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn7:
                strText = etTypedPhoneNumber.getText() + "7";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn8:
                strText = etTypedPhoneNumber.getText() + "8";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btn9:
                strText = etTypedPhoneNumber.getText() + "9";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btnAsterisk:
                strText = etTypedPhoneNumber.getText() + "*";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btnHash:
                strText = etTypedPhoneNumber.getText() + "#";
                etTypedPhoneNumber.setText(strText);
                break;

            case R.id.btnBackspace:
                strText = etTypedPhoneNumber.getText().toString();
                etTypedPhoneNumber.setText(strText.substring(0, strText.length() - 1));
                break;
        }
    }
}
