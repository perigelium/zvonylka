package ru.developer.alexangan.zvonilka.login.mvp;


import android.text.TextUtils;

import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.avosdk.Constants;
import ru.developer.alexangan.zvonilka.base.mvp.PresenterBase;
import ru.developer.alexangan.zvonilka.storage.Preferences;

public class LoginCheckPresenter extends PresenterBase<LoginContract.View> implements LoginContract.Presenter
{

    private final Preferences preferences;
    private boolean isTriedWrongLogin;
    private boolean isTriedWrongPassword;

    public LoginCheckPresenter(Preferences preferences)
    {
        this.preferences = preferences;
        isTriedWrongLogin = false;
        isTriedWrongPassword = false;
    }

    @Override
    public void viewIsReady()
    {
        getView().hideView(R.id.tv_alert_login);
        getView().hideView(R.id.tv_alert_password);
        getView().hideView(R.id.tv_hint_login);
        getView().hideView(R.id.tv_hint_password);
        getView().focusSubmitLoginPassword();

        if(!TextUtils.isEmpty(preferences.getLogin()) && !TextUtils.isEmpty(preferences.getPrefPassword()))
        {
            getView().setLoginText(preferences.getLogin());
            getView().setPasswordText(preferences.getPrefPassword());
            Constants.SIP_USER = preferences.getLogin();
            Constants.SIP_PASS = preferences.getPrefPassword();
            onSubmitLoginPasswordClicked();
        }
    }

    @Override
    public void onSubmitLoginPasswordClicked()
    {
        String curLogin = preferences.getLogin();
        String curPassword = preferences.getPrefPassword();

        if(TextUtils.isEmpty(curLogin) && TextUtils.isEmpty(curPassword))
        {
            preferences.setPrefLogin(getView().getLoginText());
            preferences.setPrefPassword(getView().getPasswordText());
            Constants.SIP_USER = getView().getLoginText();
            Constants.SIP_PASS = getView().getPasswordText();
            return;
        }

        if (!getView().getLoginText().equals(curLogin))
        {
            isTriedWrongLogin = true;
            getView().showTextView(R.id.tv_alert_login, R.string.login_not_found, true);
            getView().showView(R.id.v_horizontal_line_login, R.drawable.horizontal_line_login_alert);

            if(!TextUtils.isEmpty(getView().getLoginText()))
            {
                getView().showTextView(R.id.tv_hint_login, R.string.hint_text_login, isTriedWrongLogin);
            }
        }

        if (!getView().getPasswordText().equals(curPassword))
        {
            isTriedWrongPassword = true;
            getView().showView(R.id.v_horizontal_line_password, R.drawable.horizontal_line_login_alert);

            if(!TextUtils.isEmpty(getView().getPasswordText()))
            {
                getView().showTextView(R.id.tv_hint_password, R.string.hint_text_password, isTriedWrongPassword);
            }

            if(TextUtils.isEmpty(getView().getPasswordText()))
            {
                getView().showTextView(R.id.tv_alert_password, R.string.empty_password_warning, true);
            }
            else
            {
                getView().showTextView(R.id.tv_alert_password, R.string.wrong_password_alert, true);
            }
        }

        if (getView().getLoginText().equals(curLogin) && getView().getPasswordText().equals(curPassword))
        {
            getView().next();
            getView().close();
        }
        else
        {
            getView().focusRootLayout();
        }
    }

    @Override
    public void OnTVPasswordReminderClicked()
    {
        // send request on password reset
    }

    @Override
    public void OnPasswordKeycodeEnter()
    {
        onSubmitLoginPasswordClicked();
    }

    @Override
    public void onLoginFieldFocused()
    {
        getView().showView(R.id.v_horizontal_line_login, R.drawable.horizontal_line_login_active);
        getView().clearLoginField();
        getView().showTextView(R.id.tv_hint_login, R.string.hint_text_login, isTriedWrongLogin);
        getView().hideView(R.id.tv_alert_login);
    }

    @Override
    public void onPasswordFieldFocused()
    {
        getView().showView(R.id.v_horizontal_line_password, R.drawable.horizontal_line_login_active);
        getView().clearPasswordField();
        getView().showTextView(R.id.tv_hint_password, R.string.hint_text_password, isTriedWrongPassword);
        getView().hideView(R.id.tv_alert_password);
    }

    @Override
    public void onLoginFieldLooseFocus()
    {
        if(TextUtils.isEmpty(getView().getLoginText()))
        {
            getView().hideView(R.id.tv_hint_login);
        }
    }

    @Override
    public void onPasswordFieldLooseFocus()
    {
        if(TextUtils.isEmpty(getView().getPasswordText()))
        {
            getView().hideView(R.id.tv_hint_password);
            getView().showTextView(R.id.tv_alert_password, R.string.empty_password_warning, true);
        }
    }
}
