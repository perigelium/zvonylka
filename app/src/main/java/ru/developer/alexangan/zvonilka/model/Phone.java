package ru.developer.alexangan.zvonilka.model;

import java.util.ArrayList;

public class Phone
{
    public String number;
    public int type;

    public Phone(String number, int type)
    {
        this.number = number;
        this.type = type;
    }
}
