package ru.developer.alexangan.zvonilka.utils;

import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

public class ViewUtils
{
    public static int getScreenWidthAndroid(Activity activity)
    {
        try
        {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            //int height = displayMetrics.heightPixels;
            int width = displayMetrics.widthPixels;

            return width;
        } catch (Exception ex)
        {
            //Utils.writeToDeviceLog(ex);
        }

        return 0;
    }

    public static void displayErrorAndroid(String errMsg, Context context, DialogInterface.OnClickListener listener)
    {
        try
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle("Error");

            builder.setMessage(errMsg);

            builder.setPositiveButton("OK", listener);

            builder.show();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void displayMessageAndroid(String title, String message, Context context, DialogInterface.OnClickListener listener)
    {
        try
        {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle(title);

            builder.setMessage(message);

            builder.setPositiveButton("OK", listener);

            builder.show();
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static int calculateZoomLevel(int screenWidth)
    {
        int zoomLevel = 1;
        try
        {
            double equatorLength = 40075004; // in meters

            double metersPerPixel = equatorLength / 256;

            while ((metersPerPixel * screenWidth) > 165000)
            {
                metersPerPixel /= 2;
                ++zoomLevel;
            }
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }

        return zoomLevel;
    }

    public static void showToastMessage(final Activity activity, final String msg)
    {
        activity.runOnUiThread(new Runnable()
        {
            public void run()
            {
                Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
            }
        });
    }

/*    public static void hideSoftKeyboard(Activity activity)
    {
        // Check if no view has focus:
        View view = activity.getCurrentFocus();

        if (view != null)
        {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }*/

    public static void hideSoftKeyboard(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null)
        {
            view = new View(activity);
        }
        if (imm != null)
        {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void hideSoftKeyboard(Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (imm != null)
        {
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void showSoftKeyboard(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null)
        {
            view = new View(activity);
        }
        if (imm != null)
        {
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static void showSoftKeyboard(Context context, View view)
    {
        if (view.requestFocus())
        {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
    }

    public static void setStatusAndNavBarTransparent(Activity activity)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
        {
            Window window = activity.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.parseColor("#000000ff"));
            window.setNavigationBarColor(Color.parseColor("#000000ff"));
            window.requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

            ActionBar actionBar = activity.getActionBar();
            if (actionBar != null)
            {
                actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000ff")));

                actionBar.setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#000000ff")));
                actionBar.setIcon(new ColorDrawable(Color.parseColor("#000000ff")));
                actionBar.setTitle("");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                {
                    actionBar.setElevation(0);
                }
            }
        }
    }
}
