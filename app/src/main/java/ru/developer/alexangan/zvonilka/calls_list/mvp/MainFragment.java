package ru.developer.alexangan.zvonilka.calls_list.mvp;


import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.os.Handler;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.CallEvent;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.model.Phone;
import ru.developer.alexangan.zvonilka.utils.ViewUtils;

import static ru.developer.alexangan.zvonilka.common.Constants.APP_PREFERENCES;

public class MainFragment extends Fragment implements View.OnClickListener, SearchView.OnQueryTextListener
{
    @BindView(R.id.fabCallNewContact)
    ImageButton fabCallNewContact;

    @BindView(R.id.llTabOther)
    LinearLayout llTabOther;

    @BindView(R.id.tvShowAllRecentCalls)
    TextView tvShowAllRecentCalls;

    @BindView(R.id.tvShowAllContacts)
    TextView tvShowAllContacts;

    @BindView(R.id.rvContacts)
    ListView rvContacts;

    @BindView(R.id.lvRecentCalls)
    ListView rvRecentCalls;

    @BindView(R.id.rvSearchResults)
    ListView rvSearchResults;

    @BindView(R.id.flContent)
    FrameLayout flContent;

    @BindView(R.id.llFooter)
    LinearLayout llFooter;

    @BindView(R.id.flGoBack)
    FrameLayout flGoBack;

    @BindView(R.id.ivZvonilkaLogo)
    ImageView ivZvonilkaLogo;

    @BindView(R.id.flOpenSearchView)
    FrameLayout flOpenSearchView;

    @BindView(R.id.svContacts)
    SearchView svContacts;

    private OnFragmentInteractionListener mListener;
    ArrayList<ContactsBookItem> contacts;
    ArrayList<ContactsBookItem> searchResContacts;
    ArrayList<ContactsBookItem> recentContacts;

    private ContactsAdapter contactsAdapter;
    private ContactsAdapter searchResultsAdapter;
    private String lastSearchString;

    public MainFragment()
    {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        contacts = new ArrayList<>();
        searchResContacts = new ArrayList<>();
        recentContacts = new ArrayList<>();
        lastSearchString = "";
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.main_fragment, container, false);
        ButterKnife.bind(this, view);

/*        int searchHintBtnId = svContacts.getContext().getResources().getIdentifier("android:id/search_mag_icon", null, null);
        int hintTextId = svContacts.getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        int closeBtnId = svContacts.getContext().getResources().getIdentifier("android:id/search_close_btn", null, null);

        ImageView closeIcon = (ImageView) svContacts.findViewById(closeBtnId);
        TextView hintTextView = (TextView) svContacts.findViewById(hintTextId);
        ImageView magImage = (ImageView) svContacts.findViewById(searchHintBtnId);*/

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        llTabOther.setOnClickListener(this);
        tvShowAllContacts.setOnClickListener(this);
        tvShowAllRecentCalls.setOnClickListener(this);
        flOpenSearchView.setOnClickListener(this);
        flGoBack.setOnClickListener(this);

        svContacts.setSubmitButtonEnabled(false);

        fabCallNewContact.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) //  ACTION_UP ! wrong icon
                {
                    mListener.openNewContactFragment(contacts);
                }
                return false;
            }
        });

        contactsAdapter = new ContactsAdapter(getActivity(), R.layout.list_item_contacts, false);
        rvContacts.setAdapter(contactsAdapter);

        rvContacts.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = contacts.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        searchResultsAdapter = new ContactsAdapter(getActivity(), R.layout.list_item_contacts, true);
        rvSearchResults.setAdapter(searchResultsAdapter);

        rvSearchResults.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = searchResContacts.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        final RecentCallsAdapter mRecentsAdapter = new RecentCallsAdapter(getActivity(), R.layout.list_item_recent_calls, false);
        rvRecentCalls.setAdapter(mRecentsAdapter);

        rvRecentCalls.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                ContactsBookItem contactsBookItem = recentContacts.get(position);
                mListener.openCalleeFragment(contactsBookItem);
            }
        });

        setViewsToDefaultMode();

        if (contacts.size() == 0 && getActivity() != null)
        {
            new Handler().post(new Runnable()
            {
                @Override
                public void run()
                {
                    contacts = getContactsBookItems(getActivity());
                    fillCallEvents(getActivity(), contacts);

                    if (contacts != null && contacts.size() != 0)
                    {
                        String lastFirstLetter = "";
                        for (ContactsBookItem contactsBookItem : contacts)
                        {
                            String nameFirstLetter = contactsBookItem.displayName.substring(0, 1).toUpperCase();

                            if (!nameFirstLetter.equals(lastFirstLetter))
                            {
                                lastFirstLetter = nameFirstLetter;
                            } else
                            {
                                nameFirstLetter = "";
                            }
                            contactsBookItem.nameFirstLetter = nameFirstLetter;
                        }

                        contactsAdapter.setItems(contacts);

                        for (ContactsBookItem contactsBookItem : contacts)
                        {
                            if (contactsBookItem.calls != null && contactsBookItem.calls.size() != 0)
                            {
                                recentContacts.add(contactsBookItem);
                            }
                        }

                        if (recentContacts.size() != 0)
                        {
                            mRecentsAdapter.setItems(recentContacts);
                        }
                    }
                }
            });
        } else
        {
            contactsAdapter.setItems(contacts);
            mRecentsAdapter.setItems(recentContacts);
        }

        //closeIcon.setImageResource(R.drawable.clearsearch_2);
        //magImage.setLayoutParams(new LinearLayout.LayoutParams(0, 0));
    }

    @Override
    public void onClick(View view)
    {
        switch (view.getId())
        {
            case R.id.flOpenSearchView:
                openSearchView();
                break;

            case R.id.flGoBack:
                setViewsToDefaultMode();
                break;

            case R.id.tvShowAllRecentCalls:
                mListener.openAllRecentCallsFragment(contacts);
                break;

            case R.id.tvShowAllContacts:
                mListener.openAllContactsFragment(contacts);
                break;

            case R.id.llTabOther:
                mListener.openUserProfileFragment();
                break;
        }
    }

    private void setViewsToDefaultMode()
    {
        svContacts.setOnQueryTextListener(null);
        flGoBack.setVisibility(View.GONE);
        rvSearchResults.setVisibility(View.GONE);
        //svContacts.setIconified(true);
        svContacts.setVisibility(View.GONE);
        flOpenSearchView.setVisibility(View.VISIBLE);
        llFooter.setVisibility(View.VISIBLE);
        flContent.setVisibility(View.VISIBLE);
        ivZvonilkaLogo.setVisibility(View.VISIBLE);
        if (mListener != null)
        {
            mListener.hideSoftKeyboard();
        }
    }

    private void openSearchView()
    {
        svContacts.setOnQueryTextListener(this);
        flOpenSearchView.setVisibility(View.GONE);
        ivZvonilkaLogo.setVisibility(View.GONE);

        svContacts.setVisibility(View.VISIBLE);
        svContacts.setIconified(false); // opens virtual keyboard
    }

    private void setViewsToSearchMode()
    {
        flGoBack.setVisibility(View.VISIBLE);
        flContent.setVisibility(View.GONE);
        llFooter.setVisibility(View.GONE);
        rvSearchResults.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onQueryTextChange(String newText)
    {
        if (newText.length() == 0 && lastSearchString.length() > 0)
        {
            setViewsToDefaultMode();
            return true;
        }

        if (newText.length() == 1 && lastSearchString.length() < 2)
        {
            setViewsToSearchMode();
        }
        updateQueriedList(newText);
        lastSearchString = newText;

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String queryString)
    {
        mListener.hideSoftKeyboard();
        updateQueriedList(queryString);

        return false;
    }

    private void updateQueriedList(String queryString)
    {
        searchResContacts.clear();
        rvSearchResults.setAdapter(null);

        if (queryString.length() == 0)
        {
            return;
        }

        String strQueryLower = queryString.toLowerCase();

        ArrayList<ContactsBookItem> contactsBookItems = new ArrayList<>();

        for (ContactsBookItem contactsBookItem : contacts)
        {
            String strDisplayName = contactsBookItem.displayName.toLowerCase();

            if (strDisplayName.contains(strQueryLower))
            {
                contactsBookItems.add(contactsBookItem);
                continue;
            }

            for (Phone phone : contactsBookItem.phones)
            {
                String number = phone.number;

                if (number.contains(strQueryLower))
                {
                    contactsBookItems.add(contactsBookItem);
                    break;
                }
            }
        }

        rvSearchResults.setAdapter(searchResultsAdapter);
        searchResContacts.addAll(contactsBookItems);
        searchResultsAdapter.setSearchMatch(strQueryLower);
        searchResultsAdapter.setItems(searchResContacts);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof ru.developer.alexangan.zvonilka.calls_list.mvp.OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }

    public ArrayList<ContactsBookItem> getContactsBookItems(Context context)
    {
        ContentResolver cr = context.getContentResolver();

        if (cr == null) return null;

        String[] projection = new String[]{ContactsContract.CommonDataKinds.Phone._ID, ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Phone.NUMBER, ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.PHOTO_URI, ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI};

        String orderBy = ContactsContract.Contacts.DISPLAY_NAME + " COLLATE LOCALIZED ASC";

        Cursor crCommonDataKinds = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, projection, null, null, orderBy);

        ArrayList<ContactsBookItem> listContacts = new ArrayList<>();

        if (crCommonDataKinds.moveToFirst())
        {
            //int idIndex = query.getColumnIndex(ContactsContract.Contacts._ID);
            int phoneIndex = crCommonDataKinds.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
            int phoneTypeIndex = crCommonDataKinds.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE);
            int nameIndex = crCommonDataKinds.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
            int thumbPhotoIndex = crCommonDataKinds.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI);
            int dispPhotoIndex = crCommonDataKinds.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI);

            do
            {
                //long contactId = query.getLong(idIndex);
                String contactDisplayName = crCommonDataKinds.getString(nameIndex);
                String contactPhone = crCommonDataKinds.getString(phoneIndex);
                int phoneType = crCommonDataKinds.getInt(phoneTypeIndex);
                Phone phone = new Phone(contactPhone, phoneType);
                ArrayList<Phone> phones = new ArrayList<>();
                phones.add(phone);
                String thumbPhoto = crCommonDataKinds.getString(thumbPhotoIndex);
                String dispPhoto = crCommonDataKinds.getString(dispPhotoIndex);
                Uri thumbPhotoUri = thumbPhoto != null ? Uri.parse(thumbPhoto) : null;
                Uri displayPhotoUri = dispPhoto != null ? Uri.parse(dispPhoto) : null;

                ContactsBookItem contact = new ContactsBookItem(contactDisplayName, phones, thumbPhotoUri, displayPhotoUri);

                for (int i = 0; i <= listContacts.size(); i++)
                {
                    if (i == listContacts.size())
                    {
                        listContacts.add(contact);
                        break;
                    }

                    ContactsBookItem contactsBookItem = listContacts.get(i);

                    if (contactsBookItem.displayName.equals(contactDisplayName))
                    {
                        for (int j = 0; j < contactsBookItem.phones.size(); j++)
                        {
                            Phone contactsBookPhone = contactsBookItem.phones.get(j);

                            if (contactsBookPhone.number.equals(contactPhone))
                            {
                                break;
                            }

                            if (j == contactsBookItem.phones.size() - 1)
                            {
                                contactsBookItem.phones.add(new Phone(contactPhone, phoneType));
                            }
                        }

                        break;
                    }
                }
            } while (crCommonDataKinds.moveToNext());
        }

        crCommonDataKinds.close();
        return listContacts;
    }

    private void fillCallEvents(Context context, List<ContactsBookItem> contacts)
    {
        //StringBuilder stringBuilder = new StringBuilder();
        String orderBy = CallLog.Calls.DATE + " DESC";
        @SuppressLint("MissingPermission") Cursor cursor = context.getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, orderBy);
        int number = cursor.getColumnIndex(CallLog.Calls.NUMBER);
        int type = cursor.getColumnIndex(CallLog.Calls.TYPE);
        int date = cursor.getColumnIndex(CallLog.Calls.DATE);
        int duration = cursor.getColumnIndex(CallLog.Calls.DURATION);

        Log.d("DEBUG", "Log size = " + String.valueOf(cursor.getCount()));

        while (cursor.moveToNext())
        {
            String strNumber = cursor.getString(number);
            int callType = cursor.getInt(type);
            long callDate = cursor.getLong(date);
            long callDuration = cursor.getLong(duration);
            String strType;

            switch (callType)
            {
                case CallLog.Calls.INCOMING_TYPE:
                    strType = "Входящий";
                    break;

                case CallLog.Calls.OUTGOING_TYPE:
                    strType = "Исходящий";
                    break;

                case CallLog.Calls.MISSED_TYPE:
                    strType = "Пропущенный";
                    break;

                case CallLog.Calls.REJECTED_TYPE:
                    strType = "Отклоненный";
                    break;

                default:
                    strType = "Сбой";
                    break;
            }

            for (int i = 0; i < contacts.size(); i++)
            {
                ContactsBookItem contactsBookItem = contacts.get(i);
                for (int j = 0; j < contactsBookItem.phones.size(); j++)
                {
                    Phone phone = contactsBookItem.phones.get(j);
                    if (phone.number.equals(strNumber))
                    {
                        CallEvent callEvent = new CallEvent(strNumber, callDate, callType, strType, callDuration);
                        if (contactsBookItem.calls == null)
                        {
                            contactsBookItem.calls = new ArrayList<>();
                        }

                        contactsBookItem.calls.add(callEvent);
                    }
                }
            }
            //stringBuilder.append("\nPhone Number:--- " + phNumber + " \nCall Type:--- " + dir + " \nCall Date:--- " + callDayTime + " \nCall duration in sec :--- " + callDuration);
            //stringBuilder.append("\n----------------------------------");
        }
        cursor.close();
        //return stringBuilder.toString();
    }
}
