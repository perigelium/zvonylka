package ru.developer.alexangan.zvonilka.avosdk;

/**
 * Created by olegrussu on 02/06/2017.
 */

public class Util {

    public static final String KEY_IS_REGISTERED = "is-registered";
    public static final String KEY_PHONE_NUMBER = "phone-number";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_MNC = "mnc";
    public static final String KEY_MCC = "mcc";
    public static final String KEY_COUNTRY_CODE = "country-code";
    public static final String KEY_ISO_CODE = "iso-code";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_SIP_PASSWORD = "sip-password";
    public static final String KEY_SIP_SERVER = "sip-server";
    public static final String KEY_SIP_PORT = "sip-port";
    public static final String KEY_STUN_SERVER = "stun-server";
    public static final String KEY_STUN_PORT = "stun-port";
    public static final String KEY_TURN_SERVER = "turn-server";
    public static final String KEY_TURN_PORT = "turn-port";
    public static final String KEY_TURN_USER = "turn-user";
    public static final String KEY_TURN_PASS = "turn-pass";
    public static final String KEY_EXPIRE_TIME = "expire-time";


}
