package ru.developer.alexangan.zvonilka.login.mvp;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.calls_list.mvp.CallListActivity;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.common.Constants;
import ru.developer.alexangan.zvonilka.storage.Preferences;
import ru.developer.alexangan.zvonilka.utils.ViewUtils;

import static ru.developer.alexangan.zvonilka.common.Constants.LoginMode.CHECK;

public class LoginActivity extends Activity implements LoginContract.View
{

    @BindView(R.id.fl_login_activity)
    FrameLayout flLoginActivity;

    @BindView(R.id.tv_hint_login)
    TextView tvHintLogin;

    @BindView(R.id.tv_hint_password)
    TextView tvHintPassword;

    @BindView(R.id.et_input_login)
    EditText etInputLogin;

    @BindView(R.id.tv_alert_login)
    TextView tvAlertLoginNotFound;

    @BindView(R.id.et_input_password)
    EditText etInputPassword;

    @BindView(R.id.tv_alert_password)
    TextView tvAlertFieldCannotBeEmpty;

    @BindView(R.id.tv_i_forgot_password)
    TextView tvIForgotPassword;

    @BindView(R.id.v_horizontal_line_login)
    View vHorizontalLineLogin;

    @BindView(R.id.v_horizontal_line_password)
    View vHorizontalLinePassword;

    @BindView(R.id.btn_submit_login_password)
    Button btn_submit_login_password;

    LoginContract.Presenter presenter;

    public static void checkLogin(Context context)
    {
        startActivity(context, CHECK);
    }

    private static void startActivity(Context context, Constants.LoginMode LoginMode)
    {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(Constants.EXTRA_MODE, LoginMode);
        context.startActivity(intent);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        ViewUtils.setStatusAndNavBarTransparent(this);

        setContentView(R.layout.login_activity);
        initView();

        // extract login screen mode from intent
        Constants.LoginMode LoginMode = (Constants.LoginMode) getIntent().getSerializableExtra(Constants.EXTRA_MODE);

        presenter = provideLoginPresenter(LoginMode);

        // attach view to presenter
        presenter.attachView(this);

        // view is ready to work
        presenter.viewIsReady();
    }

    private void initView()
    {
        ButterKnife.bind(this);

        tvIForgotPassword.setPaintFlags(tvIForgotPassword.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        etInputLogin.setOnFocusChangeListener(new View.OnFocusChangeListener()
    {
        @Override
        public void onFocusChange(View view, boolean b)
        {
            if(view.hasFocus())
            {
                presenter.onLoginFieldFocused();
                ViewUtils.showSoftKeyboard(LoginActivity.this, view);
            }
            else
            {
                presenter.onLoginFieldLooseFocus();
            }
        }
    });

        etInputPassword.setOnFocusChangeListener(new View.OnFocusChangeListener()
        {
            @Override
            public void onFocusChange(View view, boolean b)
            {
                if(view.hasFocus())
                {
                    presenter.onPasswordFieldFocused();
                    ViewUtils.showSoftKeyboard(LoginActivity.this, view);
                }
                else
                {
                    presenter.onPasswordFieldLooseFocus();
                }
            }
        });

        etInputPassword.setOnEditorActionListener(new TextView.OnEditorActionListener()
        {
            @Override
            public boolean onEditorAction(TextView view, int actionId, KeyEvent event)
            {
                int result = actionId & EditorInfo.IME_MASK_ACTION;
                switch (result)
                {
                    case EditorInfo.IME_ACTION_DONE:
                        ViewUtils.hideSoftKeyboard(LoginActivity.this);
                        presenter.OnPasswordKeycodeEnter();
                        break;
                    case EditorInfo.IME_ACTION_NEXT:
                        // next stuff
                        break;
                }
                return false;
            }
        });

        btn_submit_login_password.setOnTouchListener(new View.OnTouchListener()
        {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent)
            {
                if(motionEvent.getAction() == MotionEvent.ACTION_UP)
                {
                    ViewUtils.hideSoftKeyboard(LoginActivity.this);
                    presenter.onSubmitLoginPasswordClicked();
                }

                return false;
            }
        });
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        presenter.detachView();
        if (isFinishing())
        {
            presenter.cleanup();
        }
    }

    @Override
    public String getLoginText()
    {
        return etInputLogin.getText().toString();
    }

    @Override
    public String getPasswordText()
    {
        return etInputPassword.getText().toString();
    }

    @Override
    public void focusSubmitLoginPassword()
    {
        findViewById(R.id.btn_submit_login_password).requestFocus();
    }

    @Override
    public void focusLogin()
    {
        etInputLogin.requestFocus();
    }

    @Override
    public void focusPassword()
    {
        etInputPassword.requestFocus();
    }

    @Override
    public void clearLoginField()
    {
        etInputLogin.setText("");
    }

    @Override
    public void clearPasswordField()
    {
        etInputPassword.setText("");
        //ViewUtils.hideSoftKeyboard(this);
    }

    @Override
    public void showMessage(int messageResId)
    {
        Toast.makeText(this, messageResId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void next()
    {
        startActivity(new Intent(this, CallListActivity.class));
    }

    @Override
    public void close()
    {
        finish();
    }

    @Override
    public void showTextView(int resId, int stringId, boolean alert)
    {
        TextView textView = findViewById(resId);
        textView.setText(stringId);

        int color;

        if (alert)
        {
            color = R.color.colorRed;
        } else
        {
            color = R.color.color77;
        }
        textView.setTextColor(getResources().getColor(color));
        textView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showView(int resId, int drawableResId)
    {
        View view = findViewById(resId);
        view.setBackground(getResources().getDrawable(drawableResId));
    }

    @Override
    public void hideView(int resId)
    {
        TextView textView = findViewById(resId);
        textView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void focusRootLayout()
    {
        findViewById(R.id.fl_login_activity).requestFocus();
    }

    @Override
    public void setPasswordText(String password)
    {
        etInputPassword.setText(password);
    }

    @Override
    public void setLoginText(String login)
    {
        etInputLogin.setText(login);
    }

    private LoginContract.Presenter provideLoginPresenter(Constants.LoginMode LoginMode)
    {
        Preferences preferences = Preferences.getInstance();
        switch (LoginMode)
        {
            case CHECK:
                return new LoginCheckPresenter(preferences);
            default:
                return null;
        }
    }

    public void OnTVPasswordReminderClick(View view)
    {
        presenter.OnTVPasswordReminderClicked();
    }

/*    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        switch (keyCode) {
            case KeyEvent.KEYCODE_ENTER:
                presenter.OnLoginKeycodeEnter();
                break;
            default:
                return super.onKeyUp(keyCode, event);
        }
        return true;
    }*/
}
