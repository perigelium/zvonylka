package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.ContentValues;
import android.text.TextUtils;

import java.util.List;

import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.common.User;
import ru.developer.alexangan.zvonilka.common.UserTable;

public class UsersPresenter implements CallListContract.Presenter
{

    private CallListActivity view;
    private final UsersModel model;

    public UsersPresenter(UsersModel model)
    {
        this.model = model;
    }

    public void attachView(CallListActivity usersActivity)
    {
        view = usersActivity;
    }

    public void detachView()
    {
        view = null;
    }

    @Override
    public void cleanup()
    {

    }


    @Override
    public void attachView(CallListContract.View mvpView)
    {

    }

    public void viewIsReady()
    {
        //addAllUsers();
        //loadUsers();
    }

    public void loadUsers()
    {
        model.loadUsers(new UsersModel.LoadUserCallback()
        {
            @Override
            public void onLoad(List<User> users)
            {
                //view.showUsers(users);

            }
        });
    }

    public void clearUsers()
    {
        view.showProgress();
        model.clearUsers(new UsersModel.CompleteCallback()
        {
            @Override
            public void onComplete()
            {
                view.hideProgress();
                loadUsers();
            }
        });
    }

}
