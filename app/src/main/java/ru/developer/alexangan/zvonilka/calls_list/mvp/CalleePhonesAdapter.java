package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.CallEvent;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.model.Phone;
import ru.developer.alexangan.zvonilka.utils.MyTextUtils;

public class CalleePhonesAdapter extends BaseAdapter
{
    private Context mContext;
    private String[] phones;
    private int layout_id;

    public CalleePhonesAdapter(Context context, int layout_id)
    {
        this.mContext = context;
        this.layout_id = layout_id;
    }

    @Override
    public int getCount()
    {
        return phones!= null ? phones.length : 0;
    }

    @Override
    public Object getItem(int i)
    {
        return i;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    public void setItems(String[] phones)
    {
        this.phones = phones;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(layout_id, parent, false);

        TextView tvCalleeNumber = row.findViewById(R.id.tvCalleeNumber);
        tvCalleeNumber.setText(phones[position]);

        return row;
    }
}
