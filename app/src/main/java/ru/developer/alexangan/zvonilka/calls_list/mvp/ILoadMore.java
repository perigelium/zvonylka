package ru.developer.alexangan.zvonilka.calls_list.mvp;

public interface ILoadMore
{
    void onLoadMore();
}
