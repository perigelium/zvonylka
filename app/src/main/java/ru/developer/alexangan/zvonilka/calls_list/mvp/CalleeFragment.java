package ru.developer.alexangan.zvonilka.calls_list.mvp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.app.Fragment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;

public class CalleeFragment extends Fragment
{
    @BindView(R.id.tvCallerFullName)
    TextView tvCallerFullName;

    @BindView(R.id.lvCallerRecents)
    ListView lvCallerRecents;

    @BindView(R.id.lvCalleePhones)
    ListView lvCalleePhones;

    @BindView(R.id.ivCallerAvatar)
    ImageView ivCallerAvatar;

    @BindView(R.id.btnCall)
    Button btnCall;

    @BindView(R.id.llGoBack)
    LinearLayout llGoBack;

    private String[] callerPhones;
    private AlertDialog alert;
    private OnFragmentInteractionListener mListener;
    private ContactsBookItem contactsBookItem;

    public CalleeFragment()
    {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        if (getActivity() instanceof OnFragmentInteractionListener)
        {
            mListener = (OnFragmentInteractionListener) getActivity();
        } else
        {
            throw new RuntimeException(getActivity().toString() + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.callee_fragment, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        llGoBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getActivity().onBackPressed();
            }
        });

        if (getArguments() != null)
        {
            contactsBookItem = (ContactsBookItem) getArguments().getSerializable("ContactsBookItem");

            if(contactsBookItem != null)
            {
                CalleeRecentCallsListAdapter calleeRecentCallsListAdapter = new CalleeRecentCallsListAdapter(getActivity(), R.layout.list_item_caller_recent_calls, contactsBookItem.calls);
                lvCallerRecents.setAdapter(calleeRecentCallsListAdapter);

                CalleePhonesAdapter calleePhonesAdapter = new CalleePhonesAdapter(getActivity(), R.layout.list_item_callee_phones);
                lvCalleePhones.setAdapter(calleePhonesAdapter);

                tvCallerFullName.setText(contactsBookItem.displayName);

                callerPhones = new String[contactsBookItem.phones.size()];
                for (int i = 0; i < contactsBookItem.phones.size(); i++)
                {
                    callerPhones[i] = contactsBookItem.phones.get(i).number;
                }

                calleePhonesAdapter.setItems(callerPhones);

                if(callerPhones.length!=0)
                {
                    btnCall.setOnTouchListener(new View.OnTouchListener()
                    {
                        @Override
                        public boolean onTouch(View view, MotionEvent motionEvent)
                        {
                            if (motionEvent.getAction() == MotionEvent.ACTION_UP)
                            {
                                if (callerPhones.length > 1)
                                {
                                    openNumberListSelectionDialog();
                                } else
                                {
                                    mListener.openOutgoingCallFragment(contactsBookItem);
                                    mListener.onUserCallEnvoked(callerPhones[0]);
                                }
                            }

                            return false;
                        }
                    });
                }

                //callerPhones = contactsBookItem.phones.toArray(new String[0]);

                //Picasso.with(getActivity()).load(avatarImageUrl).fit().into(ivCallerAvatar);

                if (contactsBookItem.displayPhotoUri != null)
                {
                    try
                    {
                        ivCallerAvatar.setImageURI(contactsBookItem.displayPhotoUri);
                    } catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void openNumberListSelectionDialog()
    {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.phone_selection_dialog, null);
        ListView listView = view.findViewById(R.id.lvCaleePhones);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l)
            {
                Log.d("DEBUG", "which = " + i);
                alert.dismiss();

                //getActivity().getFragmentManager().popBackStack();
                mListener.openOutgoingCallFragment(contactsBookItem);
                mListener.onUserCallEnvoked(callerPhones[i]);
            }
        });
        ArrayAdapter arrayAdapter = new ArrayAdapter(getActivity(), R.layout.callee_phones_list_item, R.id.tvCalleePhoneNumberItem, callerPhones)
        {
        };
        listView.setAdapter(arrayAdapter);
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity(), R.style.PhoneSelectionDialogStyle);
        adb.setView(view);
        //adb.setItems(callerPhones, myClickListener);
        adb.setNegativeButton("ОТМЕНА", new DialogInterface.OnClickListener()
        {
            public void onClick(DialogInterface dialog, int id)
            {
                alert.dismiss();
            }
        });
        alert = adb.create();
        alert.show();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        mListener = null;
    }
}
