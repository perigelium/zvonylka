package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.CallEvent;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.utils.MyTextUtils;

public class SearchByPhoneListAdapter extends BaseAdapter
{
    private Context mContext;
    private ArrayList<ContactsBookItem> contactsList;
    private int layout_id;

    public SearchByPhoneListAdapter(Context context, int layout_id, ArrayList<ContactsBookItem> contactsList)
    {
        this.mContext = context;
        this.contactsList = contactsList;
        this.layout_id = layout_id;
    }

    @Override
    public int getCount()
    {
        return contactsList!= null ? contactsList.size() : 0;
    }

    @Override
    public Object getItem(int i)
    {
        return i;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    public void setItems(ArrayList<ContactsBookItem> results)
    {
        contactsList = results;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(layout_id, parent, false);

        TextView tvContactName = row.findViewById(R.id.tvContactName);
        ImageView ivDialerSearchItemAvatar = row.findViewById(R.id.ivDialerSearchItemAvatar);
        TextView tvPhoneNumber = row.findViewById(R.id.tvPhoneNumber);

        //ContactsBookItem contactsBookItem = contactsList.get(position);
        tvContactName.setText(contactsList.get(position).displayName);
        tvPhoneNumber.setText(contactsList.get(position).phones.get(0).number);

        if (contactsList.get(position).thumbPhotoUri != null)
        {
            ivDialerSearchItemAvatar.setImageURI(contactsList.get(position).thumbPhotoUri);
        } else if (contactsList.get(position).displayPhotoUri != null)
        {
            ivDialerSearchItemAvatar.setImageURI(contactsList.get(position).displayPhotoUri);
        }

        return row;
    }
}
