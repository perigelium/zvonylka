package ru.developer.alexangan.zvonilka.calls_list.mvp;

import android.content.Context;
import android.graphics.Color;
import android.provider.CallLog;
import android.support.annotation.NonNull;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.developer.alexangan.zvonilka.R;
import ru.developer.alexangan.zvonilka.model.ContactsBookItem;
import ru.developer.alexangan.zvonilka.utils.MyTextUtils;

import static ru.developer.alexangan.zvonilka.common.Constants.APP_PREFERENCES;

public class RecentCallsAdapter extends BaseAdapter
{

    private final boolean searchMode;
    private Context mContext;
    private int layout_id;
    private List<ContactsBookItem> resultList = new ArrayList<>();
    private String searchMatch;

    public RecentCallsAdapter(Context context, int layout_id, boolean searchMode)
    {
        this.mContext = context;
        this.layout_id = layout_id;
        this.searchMode = searchMode;
    }

    public void setItems(ArrayList<ContactsBookItem> results)
    {
        resultList = results;
        notifyDataSetChanged();
    }

    public void setSearchMatch(String searchMatch)
    {
        this.searchMatch = searchMatch;
    }

    @Override
    public int getCount()
    {
        return resultList!= null ? resultList.size() : 0;
    }

    @Override
    public Object getItem(int i)
    {
        return resultList.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(layout_id, parent, false);

        TextView tvContactName = row.findViewById(R.id.tvContactName);
        ImageView ivListItemAvatar = row.findViewById(R.id.ivListItemAvatar);
        ImageView ivCallDirection = row.findViewById(R.id.ivCallDirection);
        TextView tvLastCallTime = row.findViewById(R.id.tvLastCallTime);

        ContactsBookItem contact = resultList.get(position);
        String strName = contact.displayName;
        int callDirection = contact.calls.get(0).type;
        long callTime = contact.calls.get(0).dateTime;

        if(contact.calls.size() > 1)
        {
            strName += " (" + contact.calls.size() + ")";
        }

        tvContactName.setText(strName);

        switch (callDirection)
        {
            case CallLog.Calls.INCOMING_TYPE:
                ivCallDirection.setImageResource(R.drawable.arrow_white_left);
                break;

            case CallLog.Calls.OUTGOING_TYPE:
                ivCallDirection.setImageResource(R.drawable.arrow_white_right);
                break;

            default:
                ivCallDirection.setImageResource(R.drawable.transparent21px);
                break;
        }

        String formattedDateTime = MyTextUtils.toDateTime(callTime, "dd.MM.yy HH:mm", "");
        tvLastCallTime.setText(formattedDateTime);

        //String strRelativeTime = DateUtils.getRelativeTimeSpanString(callTime).toString();

        long now = System.currentTimeMillis();
        String strRelativeTime = DateUtils.getRelativeTimeSpanString(callTime, now, DateUtils.DAY_IN_MILLIS).toString();
        tvLastCallTime.setText(strRelativeTime);

        if (contact.displayPhotoUri != null)
        {
            try
            {
                ivListItemAvatar.setImageURI(contact.displayPhotoUri);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        } else if (contact.thumbPhotoUri != null)
        {
            try
            {
                ivListItemAvatar.setImageURI(contact.thumbPhotoUri);
            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }

        if (searchMode && searchMatch != null)
        {
            String strNameLower = contact.displayName.toLowerCase();

            if (strNameLower.length() != 0 && strNameLower.contains(searchMatch))
            {
                SpannableString ssText = makeColoredString(contact.displayName, searchMatch);
                tvContactName.setText(ssText);
            }
        }
        return row;
    }

    @Override
    public int getItemViewType(int position)
    {
        return super.getItemViewType(position);
    }

    @NonNull
    private SpannableString makeColoredString(String strSource, String searchString)
    {
        String sourceStrLower = strSource.toLowerCase();

        int iStart = sourceStrLower.indexOf(searchString);
        int iEnd = iStart + searchString.length();

        Spannable spannable = new SpannableString(strSource);
        SpannableString ssText = new SpannableString(spannable);
        ssText.setSpan(new ForegroundColorSpan(Color.parseColor("#a0A13EA7")), iStart, iEnd, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ssText;
    }

/*    private class ViewHolder
    {
        public TextView tvContactName;
        public ImageView ivListItemAvatar;

        public ViewHolder(View itemView)
        {
            tvContactName = itemView.findViewById(R.id.tvContactName);
            ivListItemAvatar = itemView.findViewById(R.id.ivListItemAvatar);
        }
    }*/
}